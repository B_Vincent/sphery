# SPHERY 

SPH flexiblE simulatoR for astrophYsics aims to create a modular C++ code to simulate some astrophysics phenomenon. 

## Table of Contents
* [General information](#general-information)
* [Dependencies](#dependencies)
* [Setup](#setup)

## General information
The main features of the project should include :
* Simulation Core
	* Initialization of different fluids and shapes
	* Pressure solver
	* N-body solver
	* Viscosity solver
	* Modular integrator
	* Adjustable timestep
* Graphical Engine
    * Surface rendering 
    * Video generation
    
## Dependencies
Project is created with:
* C++ 20
* Cmake 3.12
* Eigen [![Current Version](https://img.shields.io/badge/version-Eigen_3.3-red.svg)](https://gitlab.com/libeigen/eigen/-/tree/3.3)
* spdlog [![Current Version](https://img.shields.io/badge/version-spdlog_1.8-red.svg)](https://github.com/gabime/spdlog/tree/v1.x)
* assimp [![Current Version](https://img.shields.io/badge/version-assimp_5.0.1-red.svg)](https://github.com/assimp/assimp)
* cereal [![Current Version](https://img.shields.io/badge/version-cereal_1.3-red.svg)](https://github.com/USCiLab/cereal)

### Optional dependencies
* Google Tests (included)
* Doxygen
  * Graphviz


We are using latest (from [gcc-11](https://github.com/gcc-mirror/gcc/tree/releases/gcc-11)) GCC on Debian 
Bullseye/11 to compile 
Sphery.

## Setup

Install dependencies (on Debian-based distribution) :
```bash
$ apt install g++ cmake make libeigen3-dev libspdlog-dev libcereal-dev doxygen graphviz
```
To compile this project, execute the following commands :

```bash
$ git clone https://gitlab.com/B_Vincent/sphery.git
$ cd sphery
$ mkdir build && cd build
$ cmake .. 
$ make -j8
```

If you want to compile unit tests or the documentation, add `-DBUILD_TESTING=ON` or `-DBUILD_DOC=ON` respectively to the cmake command line.
