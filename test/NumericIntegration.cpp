#include "NumericIntegration.h"

real num_int_simpson(const Kernel::Kernel &kernel, real end, unsigned number_of_points) {
    const auto x = [&kernel](real a) { return kernel.W(a); };
    return num_int_simpson(x, end, number_of_points);
}
