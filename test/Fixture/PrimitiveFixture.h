#pragma once

#include "VolumeGeneration/Primitive.h"
#include "VolumeGeneration/AssimpInterface.h"

class PrimitiveFixture : public ::testing::Test {
protected:
    void SetUp() override {
        VolumeGeneration::load_primitives_from_object("../../data/Cube.obj");
    }

    const VolumeGeneration::Triangle t_centroid{std::array<vec3, 3>{vec3{-1, 0, 0},
                                                           vec3{1, 0, 0},
                                                           vec3{0, 1, 0}},
                                       std::array<vec3, 3>{vec3{0, 0, 0},
                                                           vec3{0, 0, 0},
                                                           vec3{0, 0, 0}}};


    // t1 and t2 could be seen as two triangles of a cube
    const VolumeGeneration::Triangle t1{std::array<vec3, 3>{vec3{0, 0, 0},
                                                            vec3{0, -1, 0},
                                                            vec3{0, 0, -1}},
                                        std::array<vec3, 3>{vec3{-1, 1, 1}.normalized(),
                                                            vec3{-1, -1, 1}.normalized(),
                                                            vec3{-1, 1, -1}.normalized()}};

    const VolumeGeneration::Triangle t2{std::array<vec3, 3>{vec3{1, 0, 0},
                                                            vec3{1, -1, 0},
                                                            vec3{1, 0, -1}},
                                        std::array<vec3, 3>{vec3{1, 1, 1}.normalized(),
                                                            vec3{1, -1, 1}.normalized(),
                                                            vec3{1, 1, -1}.normalized()}};

    std::vector<VolumeGeneration::Triangle> primitives_cube{};
};
