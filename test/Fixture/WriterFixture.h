#pragma once

#include <filesystem>

#include <gtest/gtest.h>

class WriterFixture : public ::testing::Test {
protected:
    void TearDown() override {
        std::filesystem::remove("test.txt");
    }
};