#pragma once

#include "gtest/gtest.h"

#include "Simulator/Kernel/Kernel.h"
#include "Simulator/Kernel/LookUpTable.h"

class KernelFixture : public ::testing::Test {
protected:
    real h{1};

    Kernel::GaussianKernel gaussian_kernel{h};
    Kernel::Poly6 poly_6{h};
    Kernel::BetaSpline beta_spline{h / 2};
    Kernel::Force force{h / 2};
    Kernel::Viscosity viscosity{h};
};

class LookUpTables : public ::testing::Test {
protected:
    const real h{1};
    const real begin{0};
    const real end{1};
    const unsigned nb_points{100};

    Kernel::GaussianKernel kernel{h};
    Kernel::LookUpTable lut{&kernel, begin, end, nb_points};
};