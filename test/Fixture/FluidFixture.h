#pragma once

#include <memory>

#include "Simulator/Descriptor.h"
#include "Simulator/Integrator/IntegratorVerlet.h"
#include "Simulator/MemoryManager.h"

class FluidFixture : public ::testing::Test {
protected:
    void SetUp() override {
        mm = std::make_shared<MemoryManager>(PropertySet::Basic | PropertySet::Verlet);

        auto data = std::make_shared<DataSet>(mm);

        auto integrator = std::make_shared<IntegratorVerlet>(mm);

        descriptor = std::shared_ptr<Descriptor>(new Descriptor{data, integrator, mm, {}});
    }

    std::shared_ptr<MemoryManager> mm{};
    std::shared_ptr<Descriptor> descriptor{};

    VolumeGeneration::SphereDescriptor sd {{} , .2, .5};
    const FluidPhysicProperties fpp{.rho0=1,
            .particle_mass=200,
            .fluid_mass={},
            .speed_of_sound=100,
            .jiggle_on_h=.3,
    };
};