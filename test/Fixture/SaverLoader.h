#pragma once

#include <filesystem>

#include <gtest/gtest.h>

#include "Simulator/ParticleSystem.h"

class SaverLoader : public ::testing::Test {
protected:
    void SetUp() override {
        ps_out.add_fluid({.86e2, 20e6, {}, 1e3, ps_out.get_h() / 64},
                         VolumeGeneration::SphereDescriptor{vec3{}, ps_out.get_h() * 2, 100});
    }

    void TearDown() override {
        std::filesystem::remove("test.txt");
    }

    ParticleSystem ps_out{PropertySet::Empty | PropertySet::Verlet | PropertySet::DetailedForces};
};