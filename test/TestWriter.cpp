#include <fstream>

#include <gtest/gtest.h>

#include <cereal/archives/json.hpp>

#include "Fixture/SaverLoader.h"
#include "Fixture/WriterFixture.h"
#include "Simulator/Common.h"
#include "Simulator/ParticleSystem.h"
#include "Simulator/Saver/Loader.h"
#include "Simulator/Saver/Saver.h"

TEST_F(WriterFixture, Vec3) {
    const vec3 x_out{1, 2, 3};

    {
        std::ofstream file_o{"test.txt"};
        cereal::JSONOutputArchive OutArchive(file_o);

        OutArchive(x_out);
    }

    vec3 x_in{3, 2, 1};

    {
        std::ifstream file_i("test.txt");
        cereal::JSONInputArchive InArchive(file_i);

        InArchive(x_in);
    }

    EXPECT_EQ(x_out, x_in);
}

TEST_F(WriterFixture, VectorOfVec3) {
    const std::vector<vec3> x_out{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};

    {
        std::ofstream file_o{"test.txt"};
        cereal::JSONOutputArchive OutArchive(file_o);

        OutArchive(x_out);
    }

    std::vector<vec3> x_in{};

    {
        std::ifstream file_i{"test.txt"};
        cereal::JSONInputArchive InArchive(file_i);

        InArchive(x_in);
    }

    EXPECT_EQ(x_out, x_in);
}

TEST_F(WriterFixture, MemoryManager) {
    MemoryManager mm_out{PropertySet::Empty};
    mm_out.add_particle(100);
    mm_out.add_set(PropertySet::DetailedForces);

    {
        std::ofstream file_o{"test.txt"};
        cereal::JSONOutputArchive OutArchive(file_o);

        OutArchive(mm_out);
    }

    MemoryManager mm_in{PropertySet::Empty};

    {
        std::ifstream file_i{"test.txt"};
        cereal::JSONInputArchive InArchive(file_i);

        InArchive(mm_in);
    }

    EXPECT_EQ(mm_out, mm_in);
}

TEST_F(WriterFixture, Fluid) {
    ParticleSystem ps_out{PropertySet::Verlet};

    ps_out.add_fluid({.86e2, 20e6, {}, 1e3, ps_out.get_h() / 64},
                     VolumeGeneration::SphereDescriptor{vec3{}, ps_out.get_h() * 2, 100});

    const Fluid fluid_out = ps_out.get_fluid_by_idx(0);

    {
        std::ofstream file_o{"test.txt"};
        cereal::JSONOutputArchive OutArchive(file_o);

        OutArchive(fluid_out);
    }

    Fluid fluid_in;
    {
        std::ifstream file_i{"test.txt"};
        cereal::JSONInputArchive InArchive(file_i);

        InArchive(fluid_in);
    }

    EXPECT_EQ(fluid_out, fluid_in);
}

TEST_F(WriterFixture, ParticleSystem) {
    ParticleSystem ps_out{PropertySet::Empty | PropertySet::Verlet | PropertySet::DetailedForces};
    ps_out.add_fluid({.86e2, 20e6, {}, 1e3, ps_out.get_h() / 64},
                     VolumeGeneration::SphereDescriptor{vec3{}, ps_out.get_h() * 2, 100});

    {
        std::ofstream file_o{"test.txt"};
        cereal::JSONOutputArchive OutArchive(file_o);

        OutArchive(ps_out);
    }

    ParticleSystem ps_in{PropertySet::Empty};

    {
        std::ifstream file_i{"test.txt"};
        cereal::JSONInputArchive InArchive(file_i);

        InArchive(ps_in);
    }

    EXPECT_EQ(ps_out, ps_in);
}

TEST_F(SaverLoader, JSON) {

    Saver saver{ps_out, "test.json", SaverType::JSON};

    Loader l("test.json");
    auto ps_in = l.load();

    EXPECT_EQ(ps_out, ps_in);
}

TEST_F(SaverLoader, Binary) {

    Saver saver{ps_out, "test", SaverType::Binary};

    Loader l("test");
    auto ps_in = l.load();

    EXPECT_EQ(ps_out, ps_in);
}
