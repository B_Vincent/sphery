#include <algorithm>
#include <gtest/gtest.h>

#include "Simulator/MemoryManager.h"

#include "Fixture/FluidFixture.h"
#include "Fixture/KernelFixture.h"
#include "NumericIntegration.h"

TEST(NumericalMethods, IntegrationSimpson) {
    EXPECT_DOUBLE_EQ(num_int_simpson([]([[maybe_unused]] real a) noexcept { return 1.; }, 1, 100), 1);
    EXPECT_DOUBLE_EQ(num_int_simpson([](real a) noexcept { return a; }, 1, 100), .5);
    EXPECT_DOUBLE_EQ(num_int_simpson([](real a) noexcept { return square ::pow(a); }, 1, 100), 1. / 3.);
    EXPECT_DOUBLE_EQ(num_int_simpson([](real a) noexcept { return cube::pow(a); }, 1, 100), .25);

    // This test should fail since simpson is order 3.
    EXPECT_NE(num_int_simpson([](real a) { return natural<4>::pow(a); }, 1, 100), .2);
}

TEST_F(KernelFixture, Continuity) {
    // Kernels should be continuous meaning we expect to have W(h, h) = 0 (betaspline and force is 2h) and gradW(h, h) = 0
    // GaussianKernel works differently and don't need to undergo this test

    // Poly6
    EXPECT_EQ(poly_6.W(h), 0);
    EXPECT_EQ(poly_6.grad_W(h), 0);

    // BetaSpline   W(2h, h) = 0
    EXPECT_EQ(beta_spline.W(h), 0);
    EXPECT_EQ(beta_spline.grad_W(h), 0);

    // Force        W(2h, h) = 0
    EXPECT_EQ(force.W(h), 0);

    // Viscosity
    EXPECT_EQ(viscosity.W(h), 0);
    EXPECT_EQ(viscosity.grad_W(h), 0);
}

TEST_F(KernelFixture, ValidValueInZero) {
    // Kernels should have a valid value in zero

    // GaussianKernel
    EXPECT_TRUE(std::isfinite(gaussian_kernel.W(0)));
    EXPECT_TRUE(std::isfinite(gaussian_kernel.grad_W(0)));

    // Poly6
    EXPECT_TRUE(std::isfinite(poly_6.W(0)));
    EXPECT_TRUE(std::isfinite(poly_6.grad_W(0)));

    // BetaSpline
    EXPECT_TRUE(std::isfinite(beta_spline.W(0)));
    EXPECT_TRUE(std::isfinite(beta_spline.grad_W(0)));

    // Force
    EXPECT_TRUE(std::isfinite(force.W(0)));

    // Viscosity
    EXPECT_TRUE(std::isfinite(viscosity.W(0)));
    EXPECT_TRUE(std::isfinite(viscosity.grad_W(0)));
}

#warning Kernels Test Disabled
//TEST_F(Kernels, DISABLED_Normalization) {
//    EXPECT_DOUBLE_EQ(num_int_simpson(gaussian_kernel, 1000, 100000), 1.);
//    EXPECT_DOUBLE_EQ(num_int_simpson(poly_6, 1, 1000), 1.);
//    EXPECT_DOUBLE_EQ(num_int_simpson(beta_spline, 1, 100), 1.);
//    EXPECT_DOUBLE_EQ(num_int_simpson(force, 1, 100), 1.);
//    EXPECT_DOUBLE_EQ(num_int_simpson(viscosity, 1, 100), 1.);
//}

TEST_F(LookUpTables, Value) {
    EXPECT_NEAR(lut.W(.2), kernel.W(.2), 1e-1);
    EXPECT_NEAR(lut.W(.8), kernel.W(.8), 1e-1);
    EXPECT_NEAR(lut.W(.99), kernel.W(.99), 1e-1);

    EXPECT_NEAR(lut.grad_W(.2), kernel.grad_W(.2), 1e-1);
    EXPECT_NEAR(lut.grad_W(.8), kernel.grad_W(.8), 1e-1);
    EXPECT_NEAR(lut.grad_W(.99), kernel.grad_W(.99), 1e-1);
}

TEST_F(LookUpTables, Boundaries) {
    EXPECT_EQ(lut.W(0), kernel.W(0));
    EXPECT_EQ(lut.W(1), kernel.W(1));

    EXPECT_EQ(lut.grad_W(0), kernel.grad_W(0));
    EXPECT_EQ(lut.grad_W(1), kernel.grad_W(1));
}

TEST_F(LookUpTables, ExternalValues) {
    EXPECT_EQ(lut.W(1.2), kernel.W(1.2));
    EXPECT_EQ(lut.W(-1.2), kernel.W(-1.2));

    EXPECT_EQ(lut.grad_W(1.2), kernel.grad_W(1.2));
    EXPECT_EQ(lut.grad_W(-1.2), kernel.grad_W(-1.2));
}

TEST(MemoryManager, PropertyCreation) {
    MemoryManager mm{PropertySet::Empty};

    mm.add_property_real("name", "description");
    mm.add_property_vec3("name2", "description2");
    EXPECT_TRUE(mm.check_property_existence("name", "description"));
    EXPECT_TRUE(mm.check_property_existence("name2", "description2"));
}

TEST(MemoryManager, PropertyQuality) {
    MemoryManager mm{PropertySet::Empty};

    const unsigned nb_particles = 10;
    mm.add_particle(nb_particles);

    mm.add_property_real("name", "description");
    auto res = mm.get_real_by_name("name");
    EXPECT_EQ(res.size(), nb_particles);
}

TEST(MemoryManager, AddingParticles) {
    MemoryManager mm{PropertySet::Empty};

    mm.add_property_real("name", "description");
    auto &res = mm.get_real_by_name("name");


    const unsigned nb_particles = 10;

    EXPECT_EQ(mm.get_size(), 0);

    mm.add_particle(nb_particles);
    EXPECT_EQ(res.size(), nb_particles);
    EXPECT_EQ(mm.get_size(), nb_particles);

    mm.add_property_real("name2", "description2");
    auto &res2 = mm.get_real_by_name("name2");

    mm.add_particle(nb_particles);

//    This test fails for now because of the implementation of
//    MemoryManager, it should be reactivated soon
//    EXPECT_EQ(res.size(), nb_particles * 2);
#warning MemoryManager Test Disabled

    EXPECT_EQ(res2.size(), nb_particles * 2);
    EXPECT_EQ(mm.get_size(), nb_particles * 2);
}

TEST(MemoryManager, WrongType) {
    MemoryManager mm{PropertySet::Empty};

    mm.add_property_real("name", "description");
    mm.add_property_vec3("name2", "description2");

    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("name"));
    EXPECT_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("name"), MemoryManager::WrongType);

    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("name2"));
    EXPECT_THROW([[maybe_unused]] auto x = mm.get_real_by_name("name2"), MemoryManager::WrongType);
}

TEST(MemoryManager, PropertyNotFound) {
    MemoryManager mm{PropertySet::Empty};

    mm.add_property_real("name", "description");

    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("name"));
    EXPECT_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("name2"), MemoryManager::PropertyNotFound);
}

TEST(MemoryManager, DuplicateName) {
    MemoryManager mm{PropertySet::Empty};

    EXPECT_NO_THROW(mm.add_property_real("name", "description"));
    EXPECT_THROW(mm.add_property_real("name", "description2"), MemoryManager::DuplicateName);
    EXPECT_THROW(mm.add_property_real("name2", "description"), MemoryManager::DuplicateName);
}

TEST(MemoryManager, SetCreation) {
    MemoryManager mm{PropertySet::Basic | PropertySet::DetailedForces | PropertySet::Verlet};

    // Basic
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("mass"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("pressure"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("density"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("fluid"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("sound_speed"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("pressure_grad"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_real_by_name("density_grad"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("position"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("speed"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("force"));

    // DetailedForces
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("grav_force"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("visc_force"));
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("pres_force"));

    // Verlet
    EXPECT_NO_THROW([[maybe_unused]] auto x = mm.get_vec3_by_name("position_old"));
}

TEST(MemoryManager, Neighborhood) {
    MemoryManager mm{PropertySet::Basic};

    const unsigned nb_particles = 20;
    const unsigned id = 10;

    mm.add_particle(nb_particles);

    auto res = mm.neighborhood(10);

    const auto pair_are_ordered = [](auto &container) -> bool {
        return std::all_of(begin(container), end(container), [](const auto &pair) { return pair.first < pair.second; });
    };
    EXPECT_TRUE(pair_are_ordered(res));

    const auto has_duplicate = [](auto &container) -> bool {
        for (const auto &[first, second] : container) {
            for (const auto &[first2, second2] : container) {
                if (not((first < first2 and second < first)
                        or (first > second2 and second > second2)))
                    return false;
            }
        }
        return true;
    };
    EXPECT_FALSE(has_duplicate(res));

    const auto contain_id = [](auto &container, auto val) -> bool {
        return std::any_of(begin(container), end(container), [val](const auto &pair) { return val >= pair.first and val <= pair.second; });
    };
    EXPECT_TRUE(contain_id(res, id));
}

TEST_F(FluidFixture, Getters) {
    // Create a first fluid
    Fluid f1{28, *descriptor, fpp, sd};

    EXPECT_EQ(f1.get_rest_density(), fpp.rho0);
    EXPECT_EQ(f1.get_particle_mass(), fpp.particle_mass.value());
    EXPECT_EQ(f1.get_speed_of_sound(), fpp.speed_of_sound);
    EXPECT_EQ(f1.get_jiggle_on_h(), fpp.jiggle_on_h);
    EXPECT_EQ(f1.get_index(), 28);
}

TEST_F(FluidFixture, TotalMass) {

    // Create a first fluid
    Fluid f1{0, *descriptor, fpp, sd};

    // Create descriptor for a second fluid
    auto mm2 = std::make_shared<MemoryManager>(PropertySet::Basic | PropertySet::Verlet);
    auto data = std::make_shared<DataSet>(mm2);
    auto integrator = std::make_shared<IntegratorVerlet>(mm2);
    auto descriptor2 = std::shared_ptr<Descriptor>(new Descriptor{data, integrator, mm2, {}});

    // Adapt fpp for the second fluid
    FluidPhysicProperties fpp2{fpp};
    fpp2.particle_mass.reset();
    fpp2.fluid_mass = fpp.particle_mass.value() * static_cast<double>(mm->get_size());

    // Create the second fluid
    Fluid f2{0, *descriptor2, fpp2, sd};

    EXPECT_EQ(*mm, *mm2);
}
