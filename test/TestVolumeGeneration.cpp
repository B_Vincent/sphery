#include <algorithm>
#include <numbers>

#include <gtest/gtest.h>

#include "VolumeGeneration/BoundingVolumeHierarchy.h"
#include "VolumeGeneration/VolumeFilling.h"

#include "Fixture/PrimitiveFixture.h"

using namespace VolumeGeneration;

TEST(PrimitiveHelper, PositiveModulo) {
    const int total = 3;

    EXPECT_EQ(positive_modulo(-4, total), 2);
    EXPECT_EQ(positive_modulo(-3, total), 0);
    EXPECT_EQ(positive_modulo(-2, total), 1);
    EXPECT_EQ(positive_modulo(-1, total), 2);

    EXPECT_EQ(positive_modulo(0, total), 0);

    EXPECT_EQ(positive_modulo(1, total), 1);
    EXPECT_EQ(positive_modulo(2, total), 2);
    EXPECT_EQ(positive_modulo(3, total), 0);
    EXPECT_EQ(positive_modulo(4, total), 1);
}

TEST(PrimitiveHelper, Sign) {
    EXPECT_EQ(sign(-28), -1);
    EXPECT_EQ(sign(-1), -1);
    EXPECT_EQ(sign(0), 0);
    EXPECT_EQ(sign(1), 1);
    EXPECT_EQ(sign(28), 1);
}

TEST_F(PrimitiveFixture, Centroid) {
    EXPECT_NEAR(t_centroid.get_centroid()[0], 0, 1e-10);
    EXPECT_NEAR(t_centroid.get_centroid()[1], 1. / 3, 1e-10);
    EXPECT_NEAR(t_centroid.get_centroid()[2], 0, 1e-10);
}

TEST_F(PrimitiveFixture, FaceNormal) {
    EXPECT_NEAR(t1.get_face_normal()[0], -1, 1e-10);
    EXPECT_NEAR(t1.get_face_normal()[1], 0, 1e-10);
    EXPECT_NEAR(t1.get_face_normal()[2], 0, 1e-10);


    EXPECT_NEAR(t2.get_face_normal()[0], 1, 1e-10);
    EXPECT_NEAR(t2.get_face_normal()[1], 0, 1e-10);
    EXPECT_NEAR(t2.get_face_normal()[2], 0, 1e-10);
}

TEST_F(PrimitiveFixture, EdgesNormals) {

    EXPECT_NEAR(t1.get_edges_normals()[0][0], 0, 1e-10);
    EXPECT_NEAR(t1.get_edges_normals()[0][1], 0, 1e-10);
    EXPECT_NEAR(t1.get_edges_normals()[0][2], 1, 1e-10);

    EXPECT_NEAR(t1.get_edges_normals()[1][0], 0, 1e-10);
    EXPECT_NEAR(t1.get_edges_normals()[1][1], -1 / std::numbers::sqrt2_v<real>, 1e-10);
    EXPECT_NEAR(t1.get_edges_normals()[1][2], -1 / std::numbers::sqrt2_v<real>, 1e-10);

    EXPECT_NEAR(t1.get_edges_normals()[2][0], 0, 1e-10);
    EXPECT_NEAR(t1.get_edges_normals()[2][1], 1, 1e-10);
    EXPECT_NEAR(t1.get_edges_normals()[2][2], 0, 1e-10);


    EXPECT_NEAR(t2.get_edges_normals()[0][0], 0, 1e-10);
    EXPECT_NEAR(t2.get_edges_normals()[0][1], 0, 1e-10);
    EXPECT_NEAR(t2.get_edges_normals()[0][2], 1, 1e-10);

    EXPECT_NEAR(t2.get_edges_normals()[1][0], 0, 1e-10);
    EXPECT_NEAR(t2.get_edges_normals()[1][1], -1 / std::numbers::sqrt2_v<real>, 1e-10);
    EXPECT_NEAR(t2.get_edges_normals()[1][2], -1 / std::numbers::sqrt2_v<real>, 1e-10);

    EXPECT_NEAR(t2.get_edges_normals()[2][0], 0, 1e-10);
    EXPECT_NEAR(t2.get_edges_normals()[2][1], 1, 1e-10);
    EXPECT_NEAR(t2.get_edges_normals()[2][2], 0, 1e-10);
}

TEST_F(PrimitiveFixture, PointsNormals) {

    // Test all points of the cube
    // On a zero-centered cube, all points normals should be
    // collinear to the points vectors. Once we normalize points
    // vectors, they should be equal.

    for (const Triangle &triangle : primitives_cube) {
        const auto &points = triangle.get_points();
        const auto &points_normal = triangle.get_points_normals();
        for (unsigned i{}; i < points.size(); ++i) {
            EXPECT_EQ(points[i].normalized(), points_normal[i]);
        }
    }
}

TEST_F(PrimitiveFixture, DistanceToTriangle) {

    const vec3 p1{0, 0, 0};
    EXPECT_EQ(std::abs(signed_distance_to_triangle(p1, t1)), 0);

    const vec3 p2{-1, -.1, -.1};
    EXPECT_EQ(std::abs(signed_distance_to_triangle(p2, t1)), 1);

    const vec3 p3{0, 1, -.5};
    EXPECT_EQ(std::abs(signed_distance_to_triangle(p3, t1)), 1);

    const vec3 p4{-.1, 1, 1};
    EXPECT_EQ(std::abs(signed_distance_to_triangle(p4, t1)), p4.norm());
}

TEST_F(PrimitiveFixture, SignOfDistanceToTriangle) {

    const vec3 p1{0.1, -.1, -.1};
    const vec3 p2{-0.1, -.1, -.1};
    EXPECT_EQ(sign(signed_distance_to_triangle(p1, t1)), -1);
    EXPECT_EQ(sign(signed_distance_to_triangle(p2, t1)), 1);

    const vec3 p3{-1, 1, -.5};
    EXPECT_EQ(sign(signed_distance_to_triangle(p3, t1)), 1);

    const vec3 p4{-1, 1, -.5};
    EXPECT_EQ(sign(signed_distance_to_triangle(p4, t1)), 1);

    const vec3 p5{-.1, 1, 1};
    EXPECT_EQ(sign(signed_distance_to_triangle(p5, t1)), 1);

    const vec3 p6{-.1, 1, 1};
    EXPECT_EQ(sign(signed_distance_to_triangle(p6, t1)), 1);
}

TEST_F(PrimitiveFixture, SignedDistanceToTriangle) {

    const CuboidDescriptor cd{{0.5, -0.5, -0.5}, 0, .5, .5, .5};

    const vec3 p1{-0.1, -.1, -.1};
    EXPECT_DOUBLE_EQ(signed_distance_to_triangle(p1, t1), signed_distance_cuboid(p1, cd));

    const vec3 p2{-1, 1, -.5};
    EXPECT_DOUBLE_EQ(signed_distance_to_triangle(p2, t1), signed_distance_cuboid(p2, cd));

    const vec3 p3{-1, 1, -.5};
    EXPECT_DOUBLE_EQ(signed_distance_to_triangle(p3, t1), signed_distance_cuboid(p3, cd));

    const vec3 p4{-.1, 1, 1};
    EXPECT_DOUBLE_EQ(signed_distance_to_triangle(p4, t1), signed_distance_cuboid(p4, cd));

    const vec3 p5{-.1, 1, 1};
    EXPECT_DOUBLE_EQ(signed_distance_to_triangle(p5, t1), signed_distance_cuboid(p5, cd));
}

TEST(BoundingVolumeHierarchy, GetMaxDim) {

    Box box0(vec3{-2, -1, -1}, vec3{2, 1, 1});
    EXPECT_EQ(get_max_dim(box0), 0);

    Box box1(vec3{-1, -2, -1}, vec3{1, 2, 1});
    EXPECT_EQ(get_max_dim(box1), 1);

    Box box2(vec3{-1, -1, -2}, vec3{1, 1, 2});
    EXPECT_EQ(get_max_dim(box2), 2);

    // Equality
    Box box_e(vec3{-1, -1, -1}, vec3{1, 1, 1});
    EXPECT_EQ(get_max_dim(box_e), 2);
}

TEST(BoundingVolumeHierarchy, Cube) {
    const BoundingVolumeHierarchy<Triangle> BVH{"../../data/Cube.obj"};
    const CuboidDescriptor cd{{0, 0, 0}, 0, 1, 1, 1};

    const vec3 p1{-0.1, -.1, -.1};
    EXPECT_DOUBLE_EQ(BVH.signed_distance(p1), signed_distance_cuboid(p1, cd));
    EXPECT_DOUBLE_EQ(BVH.signed_distance(-p1), signed_distance_cuboid(-p1, cd));

    const vec3 p2{-1, 1, -.5};
    EXPECT_DOUBLE_EQ(BVH.signed_distance(p2), signed_distance_cuboid(p2, cd));
    EXPECT_DOUBLE_EQ(BVH.signed_distance(-p2), signed_distance_cuboid(-p2, cd));

    const vec3 p3{-1, 1, -.5};
    EXPECT_DOUBLE_EQ(BVH.signed_distance(p3), signed_distance_cuboid(p3, cd));
    EXPECT_DOUBLE_EQ(BVH.signed_distance(-p3), signed_distance_cuboid(-p3, cd));

    const vec3 p4{-.1, 1, 1};
    EXPECT_DOUBLE_EQ(BVH.signed_distance(p4), signed_distance_cuboid(p4, cd));
    EXPECT_DOUBLE_EQ(BVH.signed_distance(-p4), signed_distance_cuboid(-p4, cd));

    const vec3 p5{-.1, 1, 1};
    EXPECT_DOUBLE_EQ(BVH.signed_distance(p5), signed_distance_cuboid(p5, cd));
    EXPECT_DOUBLE_EQ(BVH.signed_distance(-p5), signed_distance_cuboid(-p5, cd));

    // This data are 100 randomly generated test case
    // Turns out that the 5 previous points wasn't
    // sufficient to cover all possibilities
    const std::vector<vec3> test_data {
            vec3{1.77, 0.75, 0.78}, vec3{0.29, 1.02, 0.41}, vec3{0.17, 1.63, 0.69}, vec3{0.9, 1.05, 0.41}, vec3{1.77, 1.14, 1.57},
            vec3{1.1, 0.26, 0.67}, vec3{1.27, 1.41, 0.05}, vec3{0.38, 1.33, 0.36}, vec3{0.15, 0.14, 1.46}, vec3{0.06, 0.74, 1.47},
            vec3{0.38, 1.26, 0.82}, vec3{0.25, 1.05, 0.3}, vec3{1.62, 0.72, 0.66}, vec3{1.93, 1.02, 0.68}, vec3{1.7, 1.81, 0.1},
            vec3{1.76, 0.38, 1.87}, vec3{1.16, 1.29, 0.26}, vec3{0.78, 1.59, 1.06}, vec3{0.8, 1.37, 0.41}, vec3{0.96, 0.75, 1.58},
            vec3{0.2, 1.69, 0.99}, vec3{1.68, 1.57, 0.75}, vec3{1.16, 0.61, 0.78}, vec3{1.62, 1.08, 1.54}, vec3{0.33, 1.18, 1.13},
            vec3{1.99, 1.71, 1.77}, vec3{1.94, 1.38, 0.06}, vec3{1.59, 1.78, 0.47}, vec3{1.48, 0.66, 1.78}, vec3{1.62, 0.51, 1.04},
            vec3{1.6, 1.55, 0.38}, vec3{0.31, 1.14, 1.19}, vec3{0.66, 0.34, 0.0}, vec3{1.64, 0.7, 0.96}, vec3{0.7, 1.43, 1.7},
            vec3{1.84, 0.72, 0.18}, vec3{0.05, 1.64, 1.99}, vec3{0.55, 1.05, 1.78}, vec3{0.37, 1.03, 0.39}, vec3{0.6, 1.99, 1.21},
            vec3{1.75, 1.27, 1.93}, vec3{1.73, 1.05, 1.77}, vec3{1.65, 1.2, 1.06}, vec3{0.41, 1.14, 0.61}, vec3{1.51, 0.04, 0.91},
            vec3{1.16, 0.07, 0.59}, vec3{0.47, 1.06, 0.88}, vec3{1.73, 0.14, 0.01}, vec3{0.62, 1.93, 1.29}, vec3{0.02, 1.03, 1.34},
            vec3{1.83, 1.08, 1.67}, vec3{1.06, 1.83, 1.17}, vec3{1.11, 0.05, 1.1}, vec3{0.71, 0.99, 0.82}, vec3{1.93, 0.33, 1.64},
            vec3{0.54, 0.76, 1.56}, vec3{0.28, 0.62, 1.94}, vec3{1.6, 1.99, 0.01}, vec3{1.6, 0.98, 1.09}, vec3{0.7, 0.56, 0.36},
            vec3{0.35, 0.58, 1.11}, vec3{1.28, 1.07, 1.97}, vec3{0.35, 0.52, 1.13}, vec3{1.68, 0.92, 0.4}, vec3{1.02, 0.49, 1.46},
            vec3{0.84, 0.57, 1.29}, vec3{1.53, 1.75, 0.64}, vec3{1.5, 1.79, 1.18}, vec3{1.36, 1.69, 0.41}, vec3{1.12, 0.81, 1.81},
            vec3{0.9, 0.11, 0.1}, vec3{0.26, 1.62, 0.52}, vec3{1.9, 0.43, 0.58}, vec3{1.5, 0.31, 0.57}, vec3{0.62, 1.06, 1.76},
            vec3{1.15, 1.1, 0.28}, vec3{1.37, 1.77, 1.01}, vec3{1.3, 1.15, 0.59}, vec3{1.51, 0.26, 0.51}, vec3{1.8, 0.56, 0.67},
            vec3{0.39, 1.95, 0.26}, vec3{1.76, 0.39, 0.88}, vec3{0.36, 1.94, 0.29}, vec3{1.04, 1.62, 0.6}, vec3{1.85, 0.92, 2.0},
            vec3{1.08, 0.4, 0.6}, vec3{1.63, 1.69, 0.01}, vec3{0.39, 0.47, 0.12}, vec3{1.23, 0.25, 1.34}, vec3{1.04, 1.68, 0.61},
            vec3{0.88, 0.72, 1.28}, vec3{0.32, 0.39, 1.11}, vec3{0.78, 1.12, 0.99}, vec3{1.74, 1.11, 0.99}, vec3{1.71, 0.03, 0.89},
            vec3{0.14, 0.8, 0.03}, vec3{1.82, 0.85, 1.06}, vec3{0.41, 0.4, 1.42}, vec3{1.83, 1.51, 0.26}, vec3{0.21, 0.21, 1.76},
    };

    for (unsigned i{}; i < test_data.size(); ++i) {
        EXPECT_DOUBLE_EQ(BVH.signed_distance(test_data[i]), signed_distance_cuboid(test_data[i], cd));
    }
}
