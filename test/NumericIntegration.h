#include <vector>

#include "Simulator/Common.h"
#include "Simulator/Kernel/Kernel.h"

template<typename T>
concept Function = requires(T a, real b) {
    { a(b) } -> std::convertible_to<real>;
};

template <Function T>
real num_int_simpson(const T &f, real end, unsigned number_of_points) {
    std::vector<real> x{};
    for (unsigned i = 0; i < number_of_points; ++i) {
        x.push_back(0 + i * end / (number_of_points - 1));
    }
    real sum = 0;
    for (unsigned i = 0; i < number_of_points - 1; ++i) {
        sum += (1. / 6.) * (x[i + 1] - x[i]) * (f(x[i]) + 4 * f((x[i] + x[i + 1]) / 2) + f(x[i + 1]));
    }
    return sum;
}
real num_int_simpson(const Kernel::Kernel &kernel, real end, unsigned number_of_points);
