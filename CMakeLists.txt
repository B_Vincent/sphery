cmake_minimum_required(VERSION 3.12)
project(Sphery)

option(BUILD_TESTING "Build UnitTests" OFF)
option(BUILD_DOC "Build documentation" OFF)

include_directories(${PROJECT_SOURCE_DIR}/src)

set(CompilerWarnings -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wdisabled-optimization
        -Wformat=2 -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept
        -Woverloaded-virtual -Wredundant-decls -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow
        -Wswitch-default -Wundef -Weffc++ -Wconversion -Wrange-loop-construct)

find_package(spdlog 1.8 REQUIRED)
find_package(Eigen3 3.3 REQUIRED)

macro(target_opt_and_lib library_name)
    target_compile_options(${library_name} PUBLIC ${CompilerWarnings})

    target_link_libraries(${library_name} PUBLIC spdlog::spdlog)

    target_link_libraries(${library_name} PUBLIC Eigen3::Eigen)
    target_compile_definitions(${library_name} PRIVATE EIGEN_INITIALIZE_MATRICES_BY_ZERO)

    set_property(TARGET ${library_name} PROPERTY CXX_STANDARD 20)
    set_property(TARGET ${library_name} PROPERTY CMAKE_CXX_STANDARD_REQUIRED ON)
endmacro()

add_subdirectory(src)

if (BUILD_TESTING)
    enable_testing()
    add_subdirectory(test)
endif (BUILD_TESTING)

if (BUILD_DOC)
    find_package(Doxygen REQUIRED dot)

    include(CMakeDoxygen.txt)

    doxygen_add_docs(Doxygen ALL
            src README.md
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            COMMENT "Generating API documentation with Doxygen"
            )
endif (BUILD_DOC)
