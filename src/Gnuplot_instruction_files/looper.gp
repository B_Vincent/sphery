set output 'data'.sprintf("%03i", i).'.png'
set size ratio -1
set view equal xyz
splot 'test_'.i.'.txt' with points
i=i+1
if (i<1000) reread
