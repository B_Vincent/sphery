#pragma once

#include <cereal/types/memory.hpp>
#include <cereal/types/optional.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>

#include "Simulator/Common.h"

namespace cereal {
    template <class Archive>
    void save(Archive &archive, const vec3 &v) {
        archive(cereal::make_nvp("x", v[0]),
                cereal::make_nvp("y", v[1]),
                cereal::make_nvp("z", v[2]));
    }

    template <class Archive>
    void load(Archive &archive, vec3 &v) {
        real x{}, y{}, z{};
        archive(x, y, z);
        v = {x, y, z};
    }
}// namespace cereal