#include <filesystem>
#include <fstream>

#include "Exporter.h"
#include "Logger.h"

Exporter::Exporter(const ParticleSystem &other, const std::string &filename) :
    particleSystem(other) {
    export_to_txt(filename);
}

void Exporter::export_to_txt(const std::string &name) const {

    check_path(name);

    std::ofstream f(name);
    if (!f) {
        throw std::ios_base::failure("Unable to open : " + name);
    }
    f << "#This file is a save of a state of particleSystem. It can be plotted using gnuplot \n"
      << fmt::format("#In gnuplot : splot \"{}\"\n", name);

    const auto &position = particleSystem.get_memory_manager().get_vec3_by_name("position");

    for (const auto &vec : position) {
        f << vec.x() << '\t' << vec.y() << '\t' << vec.z() << '\n';
    }
}
