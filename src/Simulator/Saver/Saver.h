#pragma once

#include <fstream>
#include <string>

#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>

#include "SerializerHelper.h"
#include "Simulator/MemoryManager.h"
#include "Writer.h"

enum class SaverType { JSON,
                       Binary };

template <typename T>
class Saver : public Writer {
public:
    explicit Saver(const T &serializable, const std::string &filename, const SaverType st) :
        serializable{serializable} {
        switch (st) {
            default:
            case SaverType::Binary:
                save_to_binary(filename);
                break;
            case SaverType::JSON:
                save_to_json(filename);
                break;
        }
    }

    ~Saver() = default;

private:
    void save_to_json(const std::string &name) const {
        check_path(name);

        std::ofstream file(name);

        {
            cereal::JSONOutputArchive OutArchive(file);

            OutArchive(serializable);
        }
    }

    void save_to_binary(const std::string &name) const {
        check_path(name);

        std::ofstream file(name, std::ios::binary);

        {
            cereal::BinaryOutputArchive OutArchive(file);

            OutArchive(serializable);
        }
    }

    const T serializable;
};
