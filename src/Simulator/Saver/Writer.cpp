#include <filesystem>

#include "Writer.h"
#include "Logger.h"

void Writer::check_path(std::string_view filename) {
    std::filesystem::path file_path{filename};
    file_path.remove_filename();

    if (!std::filesystem::exists(file_path) and !file_path.empty()) {
        std::filesystem::create_directories(file_path);
        spdlog::trace("Create directories to save the file");
    }
}
