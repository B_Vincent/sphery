#include <filesystem>

#include "Loader.h"

Loader::Loader(std::string_view filename) :
    file_type{detectType(filename)},
    filename{filename} {}

SaverType Loader::detectType(std::string_view filename) {

    const std::string ext = std::filesystem::path(filename).extension();

    if (ext.empty() or ext == ".bin")
        return SaverType::Binary;

    if (ext == ".json")
        return SaverType::JSON;

    spdlog::warn("Unknown extension : \"{}\" defaulted to JSON format", ext);
    return SaverType::JSON;
}

ParticleSystem Loader::load() const {

    ParticleSystem tmp(PropertySet::Empty);

    switch (file_type) {
        default:
            spdlog::warn("Unknown SaverType, assuming a JSON format");
            // fall-through
        case SaverType::JSON: {
            std::ifstream file{filename};
            cereal::JSONInputArchive InArchive(file);
            InArchive(tmp);
        } break;
        case SaverType::Binary: {
            std::ifstream file{filename, std::ios::binary};
            cereal::BinaryInputArchive InArchive(file);
            InArchive(tmp);
        } break;
    }

    return tmp;
}
