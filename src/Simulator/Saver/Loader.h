#pragma once

#include <string>
#include <string_view>

#include "Saver.h"
#include "Simulator/ParticleSystem.h"

class Loader {
public:
    explicit Loader(std::string_view filename);

    [[nodiscard]] ParticleSystem load() const;

private:
    static SaverType detectType(std::string_view filename);

    const SaverType file_type;
    const std::string filename;
};
