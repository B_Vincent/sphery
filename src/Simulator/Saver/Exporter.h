#pragma once

#include <string>

#include "Simulator/ParticleSystem.h"
#include "Writer.h"

class Exporter : public Writer {
public:
    explicit Exporter(const ParticleSystem &particleSystem, const std::string &filename);

    ~Exporter() = default;

private:
    void export_to_txt(const std::string &name) const;

    const ParticleSystem particleSystem;
};
