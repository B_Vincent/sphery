#pragma once

#include <string_view>

class Writer {
protected:
    static void check_path(std::string_view filename);
};
