#pragma once

#include <memory>
#include <optional>

#include <cereal/access.hpp>

#include "Common.h"
#include "Integrator/AddParticlesMethod.h"
#include "MemoryManager.h"
#include "PostProcess.h"
#include "Saver/SerializerHelper.h"
#include "VolumeGeneration/VolumeFilling.h"

class Descriptor;

struct Rotation {
    bool operator==(const Rotation &other) const = default;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(cereal::make_nvp("Axis", axis),
           cereal::make_nvp("Angle", angle));
    }

    vec3 axis{};
    real angle{};
};

struct Modifier {
    bool operator==(const Modifier &other) const = default;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(cereal::make_nvp("Offset", offset),
           cereal::make_nvp("Rotation", rotation));
    }

    vec3 offset{};
    Rotation rotation{};
};

struct FluidModifierProperties {
    bool operator==(const FluidModifierProperties &other) const = default;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(cereal::make_nvp("Position", position_modifier),
           cereal::make_nvp("Velocity", velocity_modifier));
    }

    std::optional<Modifier> position_modifier{};
    std::optional<Modifier> velocity_modifier{};
};

struct FluidPhysicProperties {
    real rho0{};
    std::optional<real> particle_mass{};
    std::optional<real> fluid_mass{};
    real speed_of_sound{};
    real jiggle_on_h{};

    FluidModifierProperties modifier_prop{};
};

class Fluid {
public:
    Fluid(unsigned idx, const Descriptor &descriptor, const FluidPhysicProperties &props, const VolumeGeneration::SphereDescriptor &);
    Fluid(unsigned idx, const Descriptor &descriptor, const FluidPhysicProperties &props, const VolumeGeneration::CuboidDescriptor &);
    template <VolumeGeneration::SignedDistanceFunction SDF>
    Fluid(unsigned idx, const Descriptor &descriptor, const FluidPhysicProperties &props, const VolumeGeneration::ObjectDescriptor<SDF> &obj_descriptor) :
        Fluid(idx, props) {
        std::vector<vec3> object;
        VolumeGeneration::add_object(object, obj_descriptor.box, obj_descriptor.sdf, obj_descriptor.distance_particle);

        add_particles(descriptor, props, object, object, AddParticlesMethod::VerletOldPos);
    }

    Fluid() = default;

    Fluid(const Fluid &other) = default;
    Fluid &operator=(const Fluid &) = delete;
    ~Fluid() = default;

    bool operator==(const Fluid &other) const = default;

    [[nodiscard]] real get_rest_density() const;
    [[nodiscard]] real get_particle_mass() const;
    [[nodiscard]] real get_speed_of_sound() const;
    [[nodiscard]] real get_jiggle_on_h() const;
    [[nodiscard]] unsigned get_index() const;
    [[nodiscard]] const FluidModifierProperties &get_position_properties() const;
    [[nodiscard]] PostProcess get_processing_effect() const;

    void set_processing_effect(PostProcess processing_effect);
    void add_processing_effect(PostProcess processing_effect);
    void remove_processing_effect(PostProcess processing_effect);


private:
    friend class cereal::access;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(cereal::make_nvp("_Index", index),
           cereal::make_nvp("Rho_0", rho0),
           cereal::make_nvp("ParticleMass", particle_mass),
           cereal::make_nvp("SpeedOfSound", speed_of_sound),
           cereal::make_nvp("JiggleValue", jiggle_on_h),
           cereal::make_nvp("PositionProperties", position_prop),
           cereal::make_nvp("_PEffect", processing_effect));
    }

    Fluid(unsigned idx, const FluidPhysicProperties &props);

    enum class PreProcess { Nothing = 0,
                            PositionModifer = 1 << 0,
                            VelocityModifer = 1 << 1,
    };

    friend constexpr inline PreProcess operator&(PreProcess x, PreProcess y);
    friend constexpr inline PreProcess operator|(PreProcess x, PreProcess y);
    friend constexpr inline PreProcess &operator|=(PreProcess &x, PreProcess y);

    [[nodiscard]] static PreProcess find_pre_processing(const FluidPhysicProperties &);
    void compute_pre_processing_effect(std::vector<vec3> &, std::vector<vec3> &, AddParticlesMethod, PreProcess);

    [[nodiscard]] static real compute_particle_mass(const FluidPhysicProperties &, unsigned long);

    void add_particles(const Descriptor &descriptor, const FluidPhysicProperties &props, std::vector<vec3> &, std::vector<vec3> &, AddParticlesMethod);
    void add_cuboid(const Descriptor &descriptor, const FluidPhysicProperties &props, const VolumeGeneration::CuboidDescriptor &);
    void add_sphere(const Descriptor &descriptor, const FluidPhysicProperties &props, const VolumeGeneration::SphereDescriptor &);


    unsigned index{};

    real rho0{};
    real particle_mass{};
    real speed_of_sound{};

    real jiggle_on_h{};

    FluidModifierProperties position_prop{};

    PostProcess processing_effect{PostProcess::Nothing};
};
