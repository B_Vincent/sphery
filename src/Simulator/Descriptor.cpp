#include <utility>

#include "Descriptor.h"
#include "Simulator/FluidPostProcessing.h"

Descriptor::Descriptor(std::shared_ptr<DataSet> data, std::shared_ptr<Integrator> integrator,
                       std::shared_ptr<MemoryManager> mm,
                       std::vector<std::pair<std::vector<std::shared_ptr<Force::Force>>, std::vector<std::shared_ptr<Force::Effect::Effect>>>> sub_pipelines) :
    data{std::move(data)},
    integrator{std::move(integrator)},
    pipeline{std::move(mm), std::move(sub_pipelines)}

{}

void Descriptor::integrate(real timestep) const {
    integrator->integrate(data, timestep);
}

void Descriptor::compute_forces() const {
    pipeline.execute(data);
}

void Descriptor::post_processing(std::vector<Fluid> &fluids, real timestep) const {
    fluid_post_processing(*data, fluids);
    integrator->post_processing(data, pipeline, fluids, timestep);
}

void Descriptor::clean_up() const {
    data->clean_up();
}

void Descriptor::set_size(unsigned long s) {
    data->set_size(s);
}

void Descriptor::add_particles(Fluid &fluid, const std::vector<vec3> &v1, const std::vector<vec3> &v2, const AddParticlesMethod pm) const {
    integrator->add_particles(data, fluid, v1, v2, pm);
}
