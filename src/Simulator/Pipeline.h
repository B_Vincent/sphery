#pragma once

#include <unordered_set>

#include "Force/Effect/Effect.h"
#include "Force/Force.h"

class Pipeline {
    using SubPipelineForce = std::vector<std::shared_ptr<Force::Force>>;
    using SubPipelineEffect = std::vector<std::shared_ptr<Force::Effect::Effect>>;
    using SubPipeline = std::pair<SubPipelineForce, SubPipelineEffect>;

public:
    Pipeline() = delete;
    Pipeline(std::shared_ptr<MemoryManager>,
             std::vector<SubPipeline> pipelines);
    ~Pipeline() = default;

    bool operator==(const Pipeline &other) const = default;

    void execute(const std::shared_ptr<DataSet> &) const;

private:
    [[nodiscard]] std::unordered_set<unsigned> create_set() const;

    void proceed_SubPipeline(const std::shared_ptr<DataSet> &, const SubPipelineForce &pipeline_f, const SubPipelineEffect &pipeline_e) const;

    std::shared_ptr<MemoryManager> memory_manager{};
    std::vector<SubPipeline> sub_pipelines{};
};
