#include "Pipeline.h"

#include <utility>


Pipeline::Pipeline(std::shared_ptr<MemoryManager> mm,
                   std::vector<SubPipeline> pipelines) :
    memory_manager{std::move(mm)},
    sub_pipelines{std::move(pipelines)} {}

void Pipeline::execute(const std::shared_ptr<DataSet> &ds) const {
    for (const auto &[forces, effects] : sub_pipelines) {
        proceed_SubPipeline(ds, forces, effects);
    }

    ds->pre_integration();
}

void Pipeline::proceed_SubPipeline(const std::shared_ptr<DataSet> &ds, const SubPipelineForce &pipeline_f, const SubPipelineEffect &pipeline_e) const {
    std::unordered_set<unsigned> particles_not_computed = create_set();

    auto it_part_computed = particles_not_computed.begin();
    while (it_part_computed != particles_not_computed.end()) {
        for (const auto &[first, last] : memory_manager->neighborhood(*it_part_computed)) {
            for (unsigned idx_neighbor_part = first; idx_neighbor_part < last; ++idx_neighbor_part) {
                if (particles_not_computed.count(idx_neighbor_part) == 0 || idx_neighbor_part == *it_part_computed)
                    continue;

                const vec3 dist = ds->positions[idx_neighbor_part] - ds->positions[*it_part_computed];
                const real norm = dist.norm();

                for (const auto &force : pipeline_f)
                    (*force)(ds, *it_part_computed, idx_neighbor_part, dist, norm);
            }
        }
        for (const auto &effect : pipeline_e)
            (*effect)(ds, *it_part_computed);

        it_part_computed = particles_not_computed.erase(it_part_computed);
    }
}

std::unordered_set<unsigned> Pipeline::create_set() const {
    std::unordered_set<unsigned> m;
    unsigned x = 0;
    std::generate_n(std::inserter(m, m.end()), memory_manager->get_size(),
                    [&] { return x++; });
    return m;
}
