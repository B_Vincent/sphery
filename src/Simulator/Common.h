#pragma once

#include <Eigen/Eigen>
#include <spdlog/fmt/fmt.h>

using real = double;
using vec3 = Eigen::Matrix<real, 3, 1>;

using Box = Eigen::AlignedBox<real, 3>;

constexpr long magic_seed = 838072698289;

constexpr unsigned real_decimals = 4;
constexpr unsigned vec3_decimals = 2;

static const std::string real_decimals_s = std::to_string(real_decimals);
static const std::string vec3_decimals_s = std::to_string(vec3_decimals);

template <int power>
struct natural {
    static constexpr real pow(const real x) {
        return natural<power - 1>::pow(x) * x;
    }
};

template <>
struct natural<0> {
    static constexpr real pow(const real) {
        return 1;
    }
};

using square = natural<2>;
using cube = natural<3>;

namespace fmt {
    template <>
    struct formatter<vec3> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) {
            return ctx.begin();
        }

        template <typename FormatContext>
        auto format(const vec3 &v, FormatContext &ctx) {
            return fmt::format_to(ctx.out(),
                                  "({:." + vec3_decimals_s + "e}, {:." + vec3_decimals_s + "e}, {:." + vec3_decimals_s
                                          + "e})",
                                  v.x(), v.y(), v.z());
        }
    };
}// namespace fmt

template <typename T>
[[maybe_unused]]
T round(T value, int decimal) {
    const T multiplier = std::pow(10.0, decimal);
    return std::round(value * multiplier) / multiplier;
}

constexpr double double_epsilon = 1e-10;

namespace std {
    template <>
    class hash<vec3> {
    public:
        size_t operator()(const vec3 &v) const {
            const auto h1 = std::hash<::real>()(v.x());
            const auto h2 = std::hash<::real>()(v.y());
            const auto h3 = std::hash<::real>()(v.z());
            return h1 ^ (h2 << 1) ^ (h3 << 2);
        }
    };

    template <>
    class equal_to<Eigen::Matrix<double, 3, 1>> {
    public:
        bool operator()(const Eigen::Matrix<double, 3, 1> &v1, const Eigen::Matrix<double, 3, 1> &v2) const {
            return (v2 - v1).norm() < double_epsilon;
        }
    };
}// namespace std

template <typename T>
class NormBasedHash {
public:
    size_t operator()(const T &v) const {
        return 1000 * static_cast<size_t>(v.norm());
    }
};
