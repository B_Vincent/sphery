#include <random>

#include "FluidPostProcessing.h"

void fluid_post_processing(DataSet &ds, std::vector<Fluid> &fluids) {
    for (unsigned index = 0; index < fluids.size(); ++index) {
        PostProcess effect = fluids[index].get_processing_effect();

        if ((effect & PostProcess::AddJiggle) == PostProcess::AddJiggle) {
            const real jiggle_on_h = fluids[index].get_jiggle_on_h();

            // To provide true randomness use :
            // std::random_device{}()
            std::mt19937 gen(magic_seed);
            std::uniform_real_distribution<> dist(-jiggle_on_h, jiggle_on_h);

            for (unsigned i = 0; i < ds.size; ++i) {
                if (static_cast<unsigned>(ds.fluids[i]) == index) {
                    vec3 random{dist(gen), dist(gen), dist(gen)};
                    ds.positions[i] += random;
                }
            }

            fluids[index].remove_processing_effect(PostProcess::AddJiggle);
        }
    }
}
