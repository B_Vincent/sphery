#include <execution>

#include "DataSetDetailed.h"

DataSetDetailed::DataSetDetailed(const std::shared_ptr<MemoryManager> &mm) :
    DataSet{mm},
    grav_force{mm->get_vec3_by_name("grav_force")},
    visc_force{mm->get_vec3_by_name("visc_force")},
    pres_force{mm->get_vec3_by_name("pres_force")} {}


void DataSetDetailed::pre_integration() const {
    for (unsigned i = 0; i < size; ++i)
        forces[i] = grav_force[i] + visc_force[i] + pres_force[i];
}

void DataSetDetailed::clean_up() const {
    std::transform(std::execution::par_unseq, begin(grav_force), end(grav_force), begin(grav_force), vec3_to_zero);
    std::transform(std::execution::par_unseq, begin(visc_force), end(visc_force), begin(visc_force), vec3_to_zero);
    std::transform(std::execution::par_unseq, begin(pres_force), end(pres_force), begin(pres_force), vec3_to_zero);


    DataSet::clean_up();
}

std::vector<vec3> &DataSetDetailed::get_grav_force() const {
    return grav_force;
}
std::vector<vec3> &DataSetDetailed::get_visc_force() const {
    return visc_force;
}
std::vector<vec3> &DataSetDetailed::get_pres_force() const {
    return pres_force;
}
