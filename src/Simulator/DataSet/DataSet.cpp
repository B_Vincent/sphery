#include <execution>

#include "DataSet.h"

DataSet::DataSet(const std::shared_ptr<MemoryManager> &mm) :
    size{mm->get_size()},
    masses{mm->get_real_by_name("mass")},
    pressures{mm->get_real_by_name("pressure")},
    densities{mm->get_real_by_name("density")},
    fluids{mm->get_real_by_name("fluid")},
    positions{mm->get_vec3_by_name("position")},
    speeds{mm->get_vec3_by_name("speed")},
    forces{mm->get_vec3_by_name("force")},
    sound_speed{mm->get_real_by_name("sound_speed")},
    pressure_grad{mm->get_real_by_name("density_grad")},
    density_grad{mm->get_real_by_name("pressure_grad")} {}

void DataSet::set_size(unsigned long s) {
    size = s;
}

void DataSet::pre_integration() const {
    spdlog::info("Basic PreIntegration");
}

void DataSet::clean_up() const {
    std::transform(std::execution::par_unseq, begin(densities), end(densities), begin(densities), real_to_zero);
    std::transform(std::execution::par_unseq, begin(forces), end(forces), begin(forces), vec3_to_zero);

    std::transform(std::execution::par_unseq, begin(density_grad), end(density_grad), begin(density_grad), real_to_zero);
    std::transform(std::execution::par_unseq, begin(pressure_grad), end(pressure_grad), begin(pressure_grad), real_to_zero);
}

std::vector<vec3> &DataSet::get_grav_force() const {
    return forces;
}
std::vector<vec3> &DataSet::get_visc_force() const {
    return forces;
}
std::vector<vec3> &DataSet::get_pres_force() const {
    return forces;
}
