#pragma once

#include <vector>

#include "Simulator/MemoryManager.h"

class DataSet {
public:
    explicit DataSet(const std::shared_ptr<MemoryManager> &mm);
    virtual ~DataSet() = default;

    void set_size(unsigned long s);

    virtual void pre_integration() const;

    virtual void clean_up() const;

    [[nodiscard]] virtual std::vector<vec3> &get_grav_force() const;
    [[nodiscard]] virtual std::vector<vec3> &get_visc_force() const;
    [[nodiscard]] virtual std::vector<vec3> &get_pres_force() const;

    static constexpr auto vec3_to_zero = []([[maybe_unused]] const vec3 &v) -> vec3 { return {}; };
    static constexpr auto real_to_zero = []([[maybe_unused]] const real r) -> real { return 0; };

    unsigned long size;

    std::vector<real> &masses;
    std::vector<real> &pressures;
    std::vector<real> &densities;
    std::vector<real> &fluids;

    std::vector<vec3> &positions;
    std::vector<vec3> &speeds;
    std::vector<vec3> &forces;

    std::vector<real> &sound_speed;
    std::vector<real> &pressure_grad;
    std::vector<real> &density_grad;
};