#pragma once

#include "DataSet.h"

class DataSetDetailed : public DataSet {
public:
    explicit DataSetDetailed(const std::shared_ptr<MemoryManager> &mm);

    void pre_integration() const final;

    void clean_up() const final;

    [[nodiscard]] std::vector<vec3> &get_grav_force() const final;
    [[nodiscard]] std::vector<vec3> &get_visc_force() const final;
    [[nodiscard]] std::vector<vec3> &get_pres_force() const final;

private:
    std::vector<vec3> &grav_force;
    std::vector<vec3> &visc_force;
    std::vector<vec3> &pres_force;
};
