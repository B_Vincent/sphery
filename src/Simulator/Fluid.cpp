#include "Fluid.h"
#include "Descriptor.h"

Fluid::Fluid(unsigned idx, const FluidPhysicProperties &props) :
    index{idx},
    rho0{props.rho0},
    speed_of_sound{props.speed_of_sound},
    jiggle_on_h{props.jiggle_on_h},
    position_prop{props.modifier_prop} {}


Fluid::Fluid(unsigned idx,
             const Descriptor &descriptor,
             const FluidPhysicProperties &props,
             const VolumeGeneration::CuboidDescriptor &cd) :
    Fluid(idx, props) {

    add_cuboid(descriptor, props, cd);
}

Fluid::Fluid(const unsigned idx,
             const Descriptor &descriptor,
             const FluidPhysicProperties &props,
             const VolumeGeneration::SphereDescriptor &sd) :
    Fluid(idx, props) {

    add_sphere(descriptor, props, sd);
}

void Fluid::add_sphere(const Descriptor &descriptor,
                       const FluidPhysicProperties &props,
                       const VolumeGeneration::SphereDescriptor &sphereDescriptor) {

    auto local_sphere_sdf = [&](const vec3 &pos) {
        return signed_distance_sphere(pos, sphereDescriptor);
    };

    std::vector<vec3> object;
    VolumeGeneration::add_object(object, VolumeGeneration::box_from_sphere_descriptor(sphereDescriptor), local_sphere_sdf, sphereDescriptor.distance_particle);

    add_particles(descriptor, props, object, object, AddParticlesMethod::VerletOldPos);
}

void Fluid::add_cuboid(const Descriptor &descriptor,
                       const FluidPhysicProperties &props,
                       const VolumeGeneration::CuboidDescriptor &cuboidDescriptor) {

    auto local_sphere_sdf = [&](const vec3 &pos) {
        return signed_distance_cuboid(pos, cuboidDescriptor);
    };

    std::vector<vec3> object;
    VolumeGeneration::add_object(object, VolumeGeneration::box_from_cuboid_descriptor(cuboidDescriptor), local_sphere_sdf, cuboidDescriptor.distance_particle);

    add_particles(descriptor, props, object, object, AddParticlesMethod::VerletOldPos);
}

real Fluid::get_rest_density() const {
    return rho0;
}

real Fluid::get_particle_mass() const {
    return particle_mass;
}

real Fluid::get_speed_of_sound() const {
    return speed_of_sound;
}

unsigned Fluid::get_index() const {
    return index;
}

PostProcess Fluid::get_processing_effect() const {
    return processing_effect;
}

real Fluid::get_jiggle_on_h() const {
    return jiggle_on_h;
}

const FluidModifierProperties &Fluid::get_position_properties() const {
    return position_prop;
}

void Fluid::set_processing_effect(PostProcess pe) {
    processing_effect = pe;
}

void Fluid::add_processing_effect(PostProcess pe) {
    processing_effect |= pe;
}

void Fluid::remove_processing_effect(PostProcess pe) {
    processing_effect &= ~pe;
}
void Fluid::add_particles(const Descriptor &descriptor,
                          const FluidPhysicProperties &props,
                          std::vector<vec3> &v1,
                          std::vector<vec3> &v2,
                          const AddParticlesMethod apm) {

    particle_mass = compute_particle_mass(props, v1.size());

    const PreProcess pp = find_pre_processing(props);

    compute_pre_processing_effect(v1, v2, apm, pp);

    descriptor.add_particles(*this, v1, v2, apm);

    processing_effect |= PostProcess::AddJiggle;

    spdlog::info("Added an Fluid with {} particles", v1.size());
}

real Fluid::compute_particle_mass(const FluidPhysicProperties &prop, const unsigned long nb_particles) {
    if (prop.particle_mass.has_value())
        return prop.particle_mass.value();
    if (prop.fluid_mass.has_value())
        return prop.fluid_mass.value() / static_cast<double>(nb_particles);
    return 0;
}

void Fluid::compute_pre_processing_effect(std::vector<vec3>& v1, std::vector<vec3>& v2, const AddParticlesMethod apm, const PreProcess pp) {
    if ((pp & PreProcess::PositionModifer) == PreProcess::PositionModifer) {
        const auto &[offset,
                     rotation] = position_prop.position_modifier.value();

        const Eigen::Transform t(Eigen::Translation<real, 3>(offset[0], offset[1], offset[2])
                                 * Eigen::AngleAxis(rotation.angle, rotation.axis));

        for (auto &position : v1) {
            position = t * position;
        }
    }

    if ((pp & PreProcess::VelocityModifer) == PreProcess::VelocityModifer) {
        if (apm == AddParticlesMethod::VerletOldPos)
            throw std::invalid_argument(fmt::format("{} : AddParticlesMethod can't be equal to VerletOldPos", std::source_location::current().function_name()));
        if (std::addressof(v1) == std::addressof(v2))
            throw std::invalid_argument(fmt::format("{} : Two first parameters must be different", std::source_location::current().function_name()));

        const auto &[offset,
                     rotation] = position_prop.velocity_modifier.value();

        const Eigen::Transform t(Eigen::Translation<real, 3>(offset[0], offset[1], offset[2])
                                 * Eigen::AngleAxis(rotation.angle, rotation.axis));

        for (auto &velocity : v2) {
            velocity = t * velocity;
        }
    }
}

Fluid::PreProcess Fluid::find_pre_processing(const FluidPhysicProperties &prop) {
    PreProcess pp = PreProcess::Nothing;
    if (prop.modifier_prop.position_modifier.has_value())
        pp |= PreProcess::PositionModifer;
    if (prop.modifier_prop.velocity_modifier.has_value())
        pp |= PreProcess::VelocityModifer;
    return pp;
}

constexpr inline Fluid::PreProcess operator&(Fluid::PreProcess x, Fluid::PreProcess y) {
    return static_cast<Fluid::PreProcess>(
            static_cast<int>(x) & static_cast<int>(y));
}

constexpr inline Fluid::PreProcess operator|(Fluid::PreProcess x, Fluid::PreProcess y) {
    return static_cast<Fluid::PreProcess>(
            static_cast<int>(x) | static_cast<int>(y));
}

constexpr inline Fluid::PreProcess &operator|=(Fluid::PreProcess &x, Fluid::PreProcess y) { return x = x | y; }
