#include "Gravity.h"

Force::Gravity::Gravity(const std::vector<Fluid> &f, real g) :
    Force{f, nullptr},
    G{g} {}

void Force::Gravity::operator()(const std::shared_ptr<DataSet> &ds, unsigned int idx_part_computed, unsigned int idx_neighbor_part, const vec3 &dist, real norm) const {

    const vec3 grav = G * ds->masses[idx_part_computed] * ds->masses[idx_neighbor_part] * dist / cube::pow(norm);

    ds->get_grav_force()[idx_part_computed] += grav;
    ds->get_grav_force()[idx_neighbor_part] -= grav;
}
