#include "DensityGrad.h"

#include <utility>

namespace Force {

    DensityGrad::DensityGrad(const std::vector<Fluid> &f, std::shared_ptr<Kernel::Kernel> kernel) :
            Force{f, std::move(kernel)} {}

    void DensityGrad::operator()(const std::shared_ptr<DataSet> &ds, unsigned int idx_part_computed, unsigned int idx_neighbor_part, [[maybe_unused]] const vec3 &dist, real norm) const {

        const real coef = kernel->grad_W(norm);

        ds->density_grad[idx_part_computed] += coef * ds->masses[idx_neighbor_part];
        ds->density_grad[idx_neighbor_part] += coef * ds->masses[idx_part_computed];
    }

}
