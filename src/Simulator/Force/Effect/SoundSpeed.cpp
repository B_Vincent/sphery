#include "SoundSpeed.h"

Force::Effect::SoundSpeed::SoundSpeed(const std::vector<Fluid> &f) :
        Effect{f} {}

void Force::Effect::SoundSpeed::operator()([[maybe_unused]] const std::shared_ptr<DataSet> &ds, unsigned int index) const {

    // If gradients are null (i.e. there is no sound wave) we should return a null velocity
    if (ds->density_grad[index] == 0) {
        ds->sound_speed[index] = 0;
        return;
    }


    ds->sound_speed[index] = sqrt(ds->pressure_grad[index] / ds->density_grad[index]);
}
