#include "OwnDensity.h"

Force::Effect::OwnDensity::OwnDensity(const std::vector<Fluid> &f, const std::shared_ptr<Kernel::Kernel> &kernel) :
    Effect{f},
    kernel_0{kernel->W(0)} {}

void Force::Effect::OwnDensity::operator()(const std::shared_ptr<DataSet> &ds, unsigned int index) const {
    ds->densities[index] += ds->masses[index] * kernel_0;
}
