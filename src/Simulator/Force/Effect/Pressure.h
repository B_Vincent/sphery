#pragma once

#include "Effect.h"

namespace Force::Effect {

    class Pressure : public Effect {
    public:
        Pressure(const std::vector<Fluid> &f, real k);
        ~Pressure() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned) const final;

    private:
        const real k;
    };

}// namespace Force::Effect
