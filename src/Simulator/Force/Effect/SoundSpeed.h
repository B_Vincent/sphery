#pragma once

#include "Effect.h"

namespace Force::Effect {

    class SoundSpeed : public Effect {
    public:
        explicit SoundSpeed(const std::vector<Fluid> &f);
        ~SoundSpeed() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned) const final;
    };

}// namespace Force::Effect