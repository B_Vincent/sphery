#include "Pressure.h"

Force::Effect::Pressure::Pressure(const std::vector<Fluid> &f, real k) :
    Effect{f},
    k{k} {}

void Force::Effect::Pressure::operator()(const std::shared_ptr<DataSet> &ds, const unsigned int idx_part_computed) const {

    const real rho0 = fluids[static_cast<unsigned>(ds->fluids[idx_part_computed])].get_rest_density();

    ds->pressures[idx_part_computed] = k * (ds->densities[idx_part_computed] - rho0);
}
