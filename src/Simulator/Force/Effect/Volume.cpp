#include "Volume.h"

Force::Effect::Volume::Volume(const std::vector<Fluid> &f, const std::shared_ptr<MemoryManager> &mm) :
        Effect{f}, volume{mm->get_real_by_name("volume")} {}

void Force::Effect::Volume::operator()(const std::shared_ptr<DataSet> &ds, unsigned int index) const {

    volume[index] = ds->masses[index] / ds->densities[index];
}
