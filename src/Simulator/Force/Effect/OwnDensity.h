#pragma once

#include "Effect.h"
#include "Simulator/Kernel/Kernel.h"

namespace Force::Effect {

    class OwnDensity : public Effect {
    public:
        OwnDensity(const std::vector<Fluid> &f, const std::shared_ptr<Kernel::Kernel> &kernel);
        ~OwnDensity() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned) const final;

    private:
        const real kernel_0;
    };

}// namespace Force::Effect