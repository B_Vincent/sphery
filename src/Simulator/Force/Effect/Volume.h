#pragma once

#include "Effect.h"
#include "Simulator/MemoryManager.h"

namespace Force::Effect {

    class Volume : public Effect {
    public:
        explicit Volume(const std::vector<Fluid> &f, const std::shared_ptr<MemoryManager> &mm);
        ~Volume() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned) const final;

    private:
        std::vector<real> &volume;
    };

}// namespace Force::Effect