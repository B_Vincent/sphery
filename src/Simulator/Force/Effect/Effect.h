#pragma once

#include <vector>

#include "Simulator/DataSet/DataSet.h"
#include "Simulator/Fluid.h"

namespace Force::Effect {

    class Effect {
    public:
        explicit Effect(const std::vector<Fluid> &f) :
            fluids{f} {};

        virtual ~Effect() = default;

        virtual void operator()(const std::shared_ptr<DataSet> &ds, unsigned) const = 0;

    protected:
        const std::vector<Fluid> &fluids;
    };

}// namespace Force::Effect