#pragma once

#include "Force.h"
#include "Simulator/Common.h"
#include "Simulator/DataSet/DataSet.h"

namespace Force::Viscosity {

    class Monaghan1992 : public Force {
    public:
        Monaghan1992(const std::vector<Fluid> &f, const std::shared_ptr<Kernel::Kernel> &kernel, real h, real alpha, real beta);
        ~Monaghan1992() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned idx_part_computed,
                        unsigned idx_neighbor_part, const vec3 &dist, real norm) const final;

    private:
        const real h{};
        const real alpha{};
        const real beta{};
    };

}// namespace Force::Viscosity