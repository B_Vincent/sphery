#pragma once

#include "Force.h"
#include "Simulator/Common.h"
#include "Simulator/DataSet/DataSet.h"

namespace Force {

    class Gravity : public Force {
    public:
        Gravity(const std::vector<Fluid> &f, real g);
        ~Gravity() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned idx_part_computed,
                        unsigned idx_neighbor_part, const vec3 &dist, real norm) const final;

    private:
        const real G{};
    };

}// namespace Force
