#include "PressureDesbrun2010.h"

namespace Force::Pressure {

    Desbrun2010::Desbrun2010(const std::vector<Fluid> &f, const std::shared_ptr<Kernel::Kernel> &kernel) :
        Force{f, kernel} {}

    void Desbrun2010::operator()(const std::shared_ptr<DataSet> &ds, const unsigned idx_part_computed,
                                 const unsigned idx_neighbor_part, const vec3 &dist, const real norm) const {

        const real coef = kernel->grad_W(norm);

        const vec3 pressure_force =
                ds->masses[idx_neighbor_part] * ds->masses[idx_part_computed]
                * (ds->pressures[idx_neighbor_part] / square::pow(ds->densities[idx_neighbor_part])
                   + ds->pressures[idx_part_computed] / square::pow(ds->densities[idx_part_computed]))
                * coef
                * (dist / norm);

        ds->get_pres_force()[idx_part_computed] -= pressure_force;
        ds->get_pres_force()[idx_neighbor_part] += pressure_force;
    }

}// namespace Force::Pressure