#include "ViscosityMonaghan1992.h"

namespace Force::Viscosity {

    Monaghan1992::Monaghan1992(const std::vector<Fluid> &f, const std::shared_ptr<Kernel::Kernel> &kernel, const real h, const real alpha, const real beta) :
        Force{f, kernel},
        h{h}, alpha{alpha}, beta{beta} {}

    void Monaghan1992::operator()(const std::shared_ptr<DataSet> &ds, unsigned idx_part_computed,
                                  unsigned idx_neighbor_part, const vec3 &dist, real norm) const {

        const real mean_density = (ds->densities[idx_part_computed] + ds->densities[idx_neighbor_part]) / 2;
        const real mean_c = (ds->sound_speed[idx_part_computed] + ds->sound_speed[idx_neighbor_part]) / 2;

        const vec3 speed = ds->speeds[idx_part_computed] - ds->speeds[idx_neighbor_part];

        const real mu = h * (speed.dot(dist)) / (square::pow(norm) + square::pow(h) / 100);

        const real pi = [mu, mean_c, alpha = alpha, beta = beta, mean_density]() -> real {
            if (mu < 0)
                return (-mean_c * alpha * mu + beta * square::pow(mu)) / mean_density;
            return 0;
        }();

        const real coef = kernel->grad_W(norm);

        ds->get_visc_force()[idx_part_computed] -= ds->masses[idx_neighbor_part] * pi * coef * (dist / norm);
        ds->get_visc_force()[idx_neighbor_part] += ds->masses[idx_part_computed] * pi * coef * (dist / norm);
    }

}// namespace Force::Viscosity