#pragma once

#include "Force.h"
#include "Simulator/DataSet/DataSet.h"

namespace Force {

    class Density : public Force {
    public:
        Density(const std::vector<Fluid> &f, const std::shared_ptr<Kernel::Kernel> &kernel);
        ~Density() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned idx_part_computed,
                        unsigned idx_neighbor_part, const vec3 &dist, real norm) const final;
    };

}// namespace Force