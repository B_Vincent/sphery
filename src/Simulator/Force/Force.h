#pragma once

#include <utility>
#include <vector>

#include "Simulator/Fluid.h"
#include "Simulator/DataSet/DataSet.h"
#include "Simulator/Kernel/Kernel.h"

namespace Force {

    class Force {
    public:
        Force(const std::vector<Fluid> &f, std::shared_ptr<Kernel::Kernel> kernel) :
            fluids{f}, kernel{std::move(kernel)} {};

        virtual ~Force() = default;

        virtual void operator()(const std::shared_ptr<DataSet> &, unsigned, unsigned, const vec3 &, real) const = 0;

    protected:
        const std::vector<Fluid> &fluids;
        const std::shared_ptr<Kernel::Kernel> kernel;
    };

}// namespace Force
