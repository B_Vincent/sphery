#include "Density.h"

Force::Density::Density(const std::vector<Fluid> &f, const std::shared_ptr<Kernel::Kernel> &kernel) :
    Force{f, kernel} {}

void Force::Density::operator()(const std::shared_ptr<DataSet> &ds, unsigned int idx_part_computed, unsigned int idx_neighbor_part, [[maybe_unused]] const vec3 &dist, real norm) const {

    const real coef = kernel->W(norm);

    ds->densities[idx_part_computed] += ds->masses[idx_neighbor_part] * coef;
    ds->densities[idx_neighbor_part] += ds->masses[idx_part_computed] * coef;
}
