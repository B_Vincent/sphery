#include "PressureGrad.h"

#include <utility>

namespace Force {

    PressureGrad::PressureGrad(const std::vector<Fluid> &f, std::shared_ptr<Kernel::Kernel> kernel) :
        Force{f, std::move(kernel)} {}

    void PressureGrad::operator()(const std::shared_ptr<DataSet> &ds, unsigned int idx_part_computed, unsigned int idx_neighbor_part, [[maybe_unused]] const vec3 &dist, real norm) const {

        const real coef = kernel->grad_W(norm);

        ds->pressure_grad[idx_part_computed] += coef * ds->masses[idx_neighbor_part] * ds->pressures[idx_neighbor_part] / ds->densities[idx_neighbor_part];
        ds->pressure_grad[idx_neighbor_part] += coef * ds->masses[idx_part_computed] * ds->pressures[idx_part_computed] / ds->densities[idx_part_computed];
    }

}
