#pragma once

#include "Force.h"
#include "Simulator/Common.h"
#include "Simulator/DataSet/DataSet.h"

namespace Force::Pressure {

    class Desbrun2010 : public Force {
    public:
        Desbrun2010(const std::vector<Fluid> &f, const std::shared_ptr<Kernel::Kernel> &kernel);
        ~Desbrun2010() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned idx_part_computed,
                        unsigned idx_neighbor_part, const vec3 &dist, real norm) const final;
    };

}// namespace Force::Pressure
