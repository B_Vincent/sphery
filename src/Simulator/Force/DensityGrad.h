#pragma once

#include "Force.h"
#include "Simulator/Common.h"
#include "Simulator/DataSet/DataSet.h"

namespace Force {

    class DensityGrad : public Force {
    public:
        DensityGrad(const std::vector<Fluid> &f, std::shared_ptr<Kernel::Kernel> kernel);
        ~DensityGrad() override = default;

        void operator()(const std::shared_ptr<DataSet> &ds, unsigned idx_part_computed,
                        unsigned idx_neighbor_part, const vec3 &dist, real norm) const final;
    };

}// namespace Force
