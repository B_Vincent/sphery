#pragma once

#include <fstream>
#include <vector>

#include "Simulator/Common.h"

namespace Kernel {

    class LookUpTable : public Kernel {
    public:
        LookUpTable(const Kernel *_kernel, const real _begin, const real _end, const unsigned _nb_points) :
            Kernel(_kernel->get_h()),
            kernel(_kernel),
            begin{_begin},
            end{_end},
            nb_points{_nb_points},
            W_values(nb_points),
            grad_W_values(nb_points) {

            for (unsigned i{}; i < nb_points; ++i) {
                W_values[i] = kernel->W(step * i);
            }

            for (unsigned i{}; i < nb_points; ++i) {
                grad_W_values[i] = kernel->grad_W(step * i);
            }
        }

        LookUpTable(const LookUpTable &) = delete;
        LookUpTable(const LookUpTable &&) = delete;
        LookUpTable operator=(const LookUpTable &) = delete;
        LookUpTable operator=(const LookUpTable &&) = delete;


        [[nodiscard]] real W(const real val) const final {
            if (not is_within_bounds(val))
                return kernel->W(val);
            return get_mean_around(W_values, val);
        }

        [[nodiscard]] real grad_W(const real val) const final {
            if (not is_within_bounds(val))
                return kernel->grad_W(val);
            return get_mean_around(grad_W_values, val);
        }

        void save_values(const std::string &name) const {
            std::ofstream f(name);
            if (!f) {
                throw std::ios_base::failure("Unable to open : " + name);
            }
            f << "#This file is a table of a given kernel. It can be plotted using gnuplot \n"
              << fmt::format("#In gnuplot : plot '{}' using 1:2 with lines, '{}' using 1:3 with lines\n", name, name);

            real value = begin;
            for (unsigned i = 0; i < nb_points; ++i) {
                f << value << '\t' << W_values[i] << '\t' << grad_W_values[i] << '\n';
                value += step;
            }
        }

    private:
        const Kernel* kernel;
        const real begin;
        const real end;

        const unsigned nb_points;

        const real step = (end - begin) / (nb_points - 1);

        std::vector<real> W_values;
        std::vector<real> grad_W_values;

        [[nodiscard]] bool is_within_bounds(const real val) const {
            if (val < begin or val > end)
                return false;
            return true;
        }

        [[nodiscard]] real get_float_index(const real val) const {
            return (nb_points - 1) * (val - begin) / end;
        }

        [[nodiscard]] real get_mean_around(const std::vector<real> &v, const real val) const {
            const real index = get_float_index(val);

            const auto ceil = std::floor(index);
            const real w1 = index - ceil;

            if (w1 == 0)
                return v[static_cast<unsigned long>(index)];

            const auto floor = std::floor(index + 1);// We always want ceil and floor to be different
            const real w2 = floor - index;

            const real sum = w2 * v[static_cast<unsigned long>(ceil)] + w1 * v[static_cast<unsigned long>(floor)];

            return sum;
        }
    };
}// namespace Kernel
