#pragma once

#include <numbers>

#include "Logger.h"
#include "Simulator/Common.h"

constexpr real pi = std::numbers::pi_v<real>;
constexpr real two_on_pi = 2 / std::numbers::pi_v<real>;

namespace Kernel {

    class Kernel {
    public:
        explicit Kernel(real h) :
            h{h} {}
        virtual ~Kernel() = default;

        [[nodiscard]] virtual real W(real dist) const = 0;
        [[nodiscard]] virtual real grad_W(real dist) const = 0;

        [[nodiscard]] real get_h() const {
            return h;
        }

    protected:
        const real h{};
        real coef_W{1};
        real coef_grad_W{1};
    };

    /**
     * Gaussian Kernel as described in Gingold & Monaghan 1977
    */
    class GaussianKernel : public Kernel {
    public:
        explicit GaussianKernel(real h) :
            Kernel(h) {
            compute_coef();
        }

        ~GaussianKernel() override = default;

        [[nodiscard]] real W(const real dist) const override {
            return coef_W * exp(-square::pow(dist / h));
        };

        [[nodiscard]] real grad_W(const real dist) const override {
            return coef_grad_W * dist * exp(-square::pow(dist / h));
        };

    private:
        void compute_coef() {

            auto tmp = 1. / (pi * square::pow(h));
            coef_W = pow(tmp, 1.5);

            coef_grad_W = -two_on_pi * pow(1. / h, 3.5);
        }
    };

    /**
     * Poly6 kernel as described in Müller and al. 2003
    */
    class Poly6 : public Kernel {
    public:
        explicit Poly6(real h) :
            Kernel(h) {
            compute_coef();
        }

        ~Poly6() override = default;

        [[nodiscard]] real W(const real dist) const override {
            if (dist <= h) {
                return coef_W * cube::pow(square::pow(h) - square::pow(dist));
            } else {
                return 0;
            }
        };

        [[nodiscard]] real grad_W(const real dist) const override {
            if (dist < h) {
                return coef_grad_W * square::pow(square::pow(h) - square::pow(dist)) * dist;
            } else {
                return 0;
            }
        };

    private:
        void compute_coef() {

            coef_W = 315. / (64. * pi * natural<9>::pow(h));

            coef_grad_W = -945. / (32. * pi * natural<9>::pow(h));
        }
    };

    /**
     * Beta-Spline kernel as described by Monaghan & Lattanzio 1985
    */
    class BetaSpline : public Kernel {

    public:
        explicit BetaSpline(real h) :
            Kernel(h) {
            compute_coef();
        }

        ~BetaSpline() override = default;

        [[nodiscard]] real W(const real dist) const override {
            const real x = dist / h;
            if (x < 1) {
                return coef_W * (4 - 6 * square::pow(x) + 3 * cube::pow(x));
            } else if (x < 2) {
                return coef_W * cube::pow(2 - x);
            } else {
                return 0;
            }
        };

        [[nodiscard]] real grad_W(const real dist) const override {
            const real x = dist / h;
            if (x < 1) {
                return coef_grad_W * (3 * dist * (3 * dist - 4 * h));
            } else if (x < 2) {
                return coef_grad_W * (-3 * square::pow(dist - 2 * h));
            } else {
                return 0;
            }
        };

    private:
        void compute_coef() {
            coef_W = 1. / (4. * pi * cube::pow(h));

            coef_grad_W = 1. / (4. * pi * natural<6>::pow(h));
        }
    };

    /**
     * Force kernel as described by Thomas & Couchman 1992
    */
    class Force : public Kernel {
    public:
        explicit Force(real h) :
            Kernel(h) {
            compute_coef();
        }

        ~Force() override = default;

        [[nodiscard]] real W(const real dist) const override {
            const real x = dist / h;
            if (x < (2. / 3.)) {
                return coef_W * (-4 * x + 44. / 9.);
            } else if (x < 1) {
                return coef_W * (4 - 6 * square::pow(x) + 3 * cube::pow(x));
            } else if (x < 2) {
                return coef_W * cube::pow(2 - x);
            } else {
                return 0;
            }
        };

        [[deprecated("Use BetaSpline instead.")]] [[nodiscard]] real grad_W(const real) const override {
            return 0;
        };

    private:
        void compute_coef() {
            coef_W = 1. / (4. * pi * cube::pow(h));

            coef_grad_W = 0;
        }
    };

    /**
     * Viscosity kernel as described in Müller and al. 2003
     *
     * Against the source we define this kernel with W(0) = 0 and ∇W(0) = 0
    */
    class Viscosity : public Kernel {
    public:
        explicit Viscosity(real h) :
            Kernel(h) {
            compute_coef();
        }

        ~Viscosity() override = default;


        [[nodiscard]] real W(const real dist) const override {
            if (dist <= h and dist != 0) {
                return coef_W
                       * (-cube::pow(dist) / (2. * cube::pow(h)) + square::pow(dist) / square::pow(h) + h / (2. * dist)
                          - 1.);
            } else {
                return 0;
            }
        };

        [[nodiscard]] real grad_W(const real dist) const override {
            if (dist < h and dist != 0) {
                return coef_grad_W * (-3. * square::pow(dist) / (2. * cube::pow(h)) + 2. * dist / square::pow(h) - h / (2. * square::pow(dist)));
            } else {
                return 0;
            }
        };

    private:
        void compute_coef() {
            coef_W = 15. / (2. * pi * natural<3>::pow(h));

            coef_grad_W = coef_W;
        }
    };


}// namespace Kernel
