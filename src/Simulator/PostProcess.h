#pragma once

enum class PostProcess { Nothing            = 0,
                         ComputeSpeed       = 1 << 0,
                         ComputeOldPos      = 1 << 1,
                         AddJiggle          = 1 << 2,
};

constexpr inline PostProcess operator&(PostProcess x, PostProcess y) {
    return static_cast<PostProcess>(
            static_cast<int>(x) & static_cast<int>(y));
}

constexpr inline PostProcess operator|(PostProcess x, PostProcess y) {
    return static_cast<PostProcess>(
            static_cast<int>(x) | static_cast<int>(y));
}

constexpr inline PostProcess operator~(PostProcess x) { return static_cast<PostProcess>(~static_cast<int>(x)); }

constexpr inline PostProcess &operator&=(PostProcess &x, PostProcess y) { return x = x & y; }
constexpr inline PostProcess &operator|=(PostProcess &x, PostProcess y) { return x = x | y; }
