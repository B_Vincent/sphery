#pragma once

#include "Fluid.h"
#include "Simulator/DataSet/DataSet.h"

void fluid_post_processing(DataSet &ds, std::vector<Fluid> &fluids);
