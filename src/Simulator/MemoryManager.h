#pragma once

#include <optional>
#include <string>
#include <utility>
#include <vector>

#include <cereal/access.hpp>

#include "Common.h"
#include "Logger.h"
#include "Saver/SerializerHelper.h"

struct properties_helper {
    properties_helper() = default;
    properties_helper(std::string name, std::string description, std::optional<int> id_real, std::optional<int> id_vector) :
        name{std::move(name)},
        description{std::move(description)},
        id_real{id_real},
        id_vector{id_vector} {}

    bool operator==(const properties_helper &other) const = default;

    std::string name{};
    std::string description{};
    std::optional<unsigned> id_real{};
    std::optional<unsigned> id_vector{};

    real mean{};
    real standard_deviation{};

    vec3 mean_vector{};

private:
    friend class cereal::access;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(cereal::make_nvp("Name", name),
           cereal::make_nvp("Description", description),
           cereal::make_nvp("_IdR", id_real),
           cereal::make_nvp("_IdV", id_vector),
           cereal::make_nvp("_Mean", mean),
           cereal::make_nvp("_StdDev", standard_deviation),
           cereal::make_nvp("_MeanVec", mean_vector));
    }
};

enum class PropertySet {
    Empty = 0,
    Basic = 1,
    DetailedForces = 2,
    Verlet = 4,
};

constexpr PropertySet operator&(PropertySet x, PropertySet y) {
    return static_cast<PropertySet>(
            static_cast<int>(x) & static_cast<int>(y));
}

constexpr PropertySet operator|(PropertySet x, PropertySet y) {
    return static_cast<PropertySet>(
            static_cast<int>(x) | static_cast<int>(y));
}


class MemoryManager {
public:
    explicit MemoryManager(PropertySet);
    MemoryManager(const MemoryManager &other) = default;
    MemoryManager operator=(const MemoryManager &other) = delete;
    ~MemoryManager() = default;

    bool operator==(const MemoryManager &other) const = default;

    void add_property_real(const std::string &name, const std::string &description);
    void add_property_vec3(const std::string &name, const std::string &description);

    [[nodiscard]] bool check_property_existence(std::string_view name, std::string_view description) const;

    void add_set(PropertySet);

    void add_particle(unsigned long number);

    void display_properties() const;
    void display_particle(unsigned index) const;
    void display_statistics(bool update);
    void display_statistics() const;

    [[nodiscard]] std::vector<real> &get_real_by_name(std::string_view name);
    [[nodiscard]] const std::vector<real> &get_real_by_name(std::string_view name) const;

    [[nodiscard]] std::vector<vec3> &get_vec3_by_name(std::string_view name);
    [[nodiscard]] const std::vector<vec3> &get_vec3_by_name(std::string_view name) const;

    [[nodiscard]] unsigned long get_size() const;

    [[nodiscard]] std::vector<std::pair<unsigned, unsigned>> neighborhood(unsigned particle_idx) const;

    class DuplicateName : public std::exception {
    public:
        DuplicateName(const MemoryManager &mm, std::string_view name, std::string_view description);
        DuplicateName(const DuplicateName &) = delete;
        DuplicateName &operator=(const DuplicateName &) = delete;

        [[nodiscard]] const char *what() const noexcept override;

    private:
        const MemoryManager &memory_manager;
        const std::string msg;
    };

    class PropertyNotFound : public std::exception {
    public:
        PropertyNotFound(std::string_view name, std::string_view type);
        PropertyNotFound(const PropertyNotFound &) = delete;
        PropertyNotFound &operator=(const PropertyNotFound &) = delete;

        [[nodiscard]] const char *what() const noexcept override;

    private:
        const std::string msg;
    };

    class WrongType : public std::exception {
    public:
        WrongType(std::string_view name, std::string_view type);
        WrongType(const WrongType &) = delete;
        WrongType &operator=(const WrongType &) = delete;

        [[nodiscard]] const char *what() const noexcept override;

    private:
        const std::string msg;
    };

private:
    friend class cereal::access;

    MemoryManager() = default;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(cereal::make_nvp("_NbPart", number_of_particles),
           cereal::make_nvp("_Prop", properties),
           cereal::make_nvp("_VReal", real_params),
           cereal::make_nvp("_VVec", vec3_params));
    }

    void compute_statistics();

    template <typename Self>
    [[nodiscard]] static auto &get_real_by_name_impl(Self &self, std::string_view name) {
        for (const auto &prop : self.properties) {
            if (name == prop.name) {
                if (prop.id_real.has_value())
                    return self.real_params[prop.id_real.value()];
                else
                    throw WrongType{name, "real"};
            }
        }
        throw PropertyNotFound{name, "real"};
    }

    template <typename Self>
    [[nodiscard]] static auto &get_vec3_by_name_impl(Self &self, std::string_view name) {
        for (const auto &prop : self.properties) {
            if (name == prop.name) {
                if (prop.id_vector.has_value())
                    return self.vec3_params[prop.id_vector.value()];
                else
                    throw WrongType{name, "vec3"};
            }
        }
        throw PropertyNotFound{name, "vec3"};
    }

    unsigned long number_of_particles = 0;
    std::vector<properties_helper> properties{};
    std::vector<std::vector<real>> real_params{};
    std::vector<std::vector<vec3>> vec3_params{};
};
