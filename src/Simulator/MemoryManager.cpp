#include <execution>
#include <numeric>
#include <ranges>
#include <string>
#include <utility>

#include "Logger.h"
#include "MemoryManager.h"


MemoryManager::MemoryManager(PropertySet ps) {
    add_set(ps);
}

void MemoryManager::add_property_real(const std::string &name, const std::string &description) {
    if (check_property_existence(name, description))
        return;
    real_params.emplace_back(number_of_particles);
    properties.emplace_back(name, description,
                            static_cast<unsigned>(real_params.size()) - 1,
                            std::optional<unsigned>{});
}

void MemoryManager::add_property_vec3(const std::string &name, const std::string &description) {
    if (check_property_existence(name, description))
        return;
    vec3_params.emplace_back(number_of_particles);
    properties.emplace_back(name, description,
                            std::optional<unsigned>{},
                            static_cast<unsigned>(vec3_params.size()) - 1);
}

bool MemoryManager::check_property_existence(const std::string_view name, const std::string_view description) const {
    const bool name_found = std::any_of(properties.begin(),
                                        properties.end(),
                                        [&](const properties_helper &elem) { return elem.name == name; });
    const bool description_found = std::any_of(properties.begin(),
                                               properties.end(),
                                               [&](const properties_helper &elem) {
                                                   return elem.description == description;
                                               });

    if ((name_found || description_found) != (name_found && description_found)) {
        throw DuplicateName(*this, name, description);
    }

    return name_found && description_found;
}

void MemoryManager::add_set(PropertySet set) {
    if (set == PropertySet::Empty) {
        spdlog::debug("Creating an empty MemoryManager");
    }
    if ((set & PropertySet::Basic) == PropertySet::Basic) {
        add_property_real("mass", "Mass");
        add_property_real("pressure", "Pressure");
        add_property_real("density", "Density");
        add_property_real("fluid", "Index of the fluid the particle belongs to");

        add_property_vec3("position", "3D coordinate");
        add_property_vec3("speed", "Speed vector");
        add_property_vec3("force", "Force vector");

        add_property_real("sound_speed", "Speed of sound in the fluid");
        add_property_real("pressure_grad", "Pressure gradient");
        add_property_real("density_grad", "Density gradient");
    }
    if ((set & PropertySet::DetailedForces) == PropertySet::DetailedForces) {
        add_property_vec3("grav_force", "Gravitational force");
        add_property_vec3("visc_force", "Viscosity force");
        add_property_vec3("pres_force", "Pressure force");
    }
    if ((set & PropertySet::Verlet) == PropertySet::Verlet) {
        add_property_vec3("position_old", "3D coordinate at previous step, Verlet Integration specific");
    }
}

void MemoryManager::display_properties() const {
    for (const auto &prop : properties) {
        if (prop.id_real.has_value())
            spdlog::info("ID real_param : {} | {} : {}", prop.id_real.value(), prop.name, prop.description);
        if (prop.id_vector.has_value())
            spdlog::info("ID vec3_param : {} | {} : {}", prop.id_vector.value(), prop.name, prop.description);
    }
}

void MemoryManager::display_particle(unsigned int index) const {
    std::string msg = fmt::format("ID: {} ", index);
    for (const auto &prop : properties) {
        if (prop.id_real.has_value())
            msg += fmt::format(", {} : {:." + real_decimals_s + "e}", prop.name, get_real_by_name(prop.name)[index]);
        if (prop.id_vector.has_value())
            msg += fmt::format(", {} : {}", prop.name, get_vec3_by_name(prop.name)[index]);
    }
    spdlog::info(msg);
}

void MemoryManager::display_statistics(const bool update) {

    if (update)
        compute_statistics();

    display_statistics();
}

void MemoryManager::display_statistics() const {

    std::string msg{};
    for (const auto &prop : properties) {
        msg += fmt::format("{} : mean = {:." + real_decimals_s + "e}\t", prop.name, prop.mean);
    }
    spdlog::info(msg);
}

std::vector<real> &MemoryManager::get_real_by_name(const std::string_view name) {
    return get_real_by_name_impl(*this, name);
}

const std::vector<real> &MemoryManager::get_real_by_name(const std::string_view name) const {
    return get_real_by_name_impl(*this, name);
}

std::vector<vec3> &MemoryManager::get_vec3_by_name(const std::string_view name) {
    return get_vec3_by_name_impl(*this, name);
}

const std::vector<vec3> &MemoryManager::get_vec3_by_name(const std::string_view name) const {
    return get_vec3_by_name_impl(*this, name);
}

unsigned long MemoryManager::get_size() const {
    return number_of_particles;
}

void MemoryManager::add_particle(const unsigned long number) {

    const auto total = number_of_particles + number;

    for (auto &vect : real_params) {
        vect.resize(total);
    }
    for (auto &vect : vec3_params) {
        vect.resize(total);
    }

    number_of_particles = total;
}

std::vector<std::pair<unsigned int, unsigned int>> MemoryManager::neighborhood([[maybe_unused]] const unsigned int particle_idx) const {
    return {{0, number_of_particles - 1}};
}

void MemoryManager::compute_statistics() {

    const auto mean = [](const auto &container) {
#warning Eigen can not be used with auto
        const auto sum = std::reduce(std::execution::par_unseq, begin(container), end(container));

        return sum / static_cast<real>(size(container));
    };

    const auto standard_deviation = [](const auto &container, const real mean) {
        const auto sum = std::transform_reduce(
                std::execution::par_unseq,
                begin(container), end(container),
                0.0,
                std::plus<>(),
                [&mean](const real x) { return square::pow((x - mean)); });
        return sqrt(sum / static_cast<real>(size(container)));
    };

    const auto select_vector = [&mean,
                                &standard_deviation,
                                &vector_real = this->real_params,
                                &vector_vec3 = this->vec3_params](properties_helper &prop) {
        if (prop.id_real.has_value()) {
            const auto &vector = vector_real[prop.id_real.value()];
            prop.mean = mean(vector);
            prop.standard_deviation = standard_deviation(vector, prop.mean);
        }
        if (prop.id_vector.has_value()) {
            const auto &vector = vector_vec3[prop.id_vector.value()];
            const auto &vector_norm = vector | std::views::transform([](const vec3 &v) { return v.norm(); });

            prop.mean = mean(vector_norm);
            prop.mean_vector = mean(vector);
            prop.standard_deviation = standard_deviation(vector_norm, prop.mean);
        }
    };

    std::for_each(std::execution::par_unseq, begin(properties), end(properties), select_vector);
}

MemoryManager::DuplicateName::DuplicateName(const MemoryManager &mm, const std::string_view name, const std::string_view description) :
    memory_manager{mm},
    msg{fmt::format(R"(Unable to create property with name: "{}" and description: "{}")", name, description)} {}

const char *MemoryManager::DuplicateName::what() const noexcept {
    memory_manager.display_properties();
    return msg.c_str();
}

MemoryManager::PropertyNotFound::PropertyNotFound(const std::string_view name, const std::string_view type) :
    msg{fmt::format(R"({}: not found in existing properties of type {})", name, type)} {}

const char *MemoryManager::PropertyNotFound::what() const noexcept {
    return msg.c_str();
}

MemoryManager::WrongType::WrongType(const std::string_view name, const std::string_view type) :
    msg{fmt::format(R"({}: property format is not {})", name, type)} {}

const char *MemoryManager::WrongType::what() const noexcept {
    return msg.c_str();
}