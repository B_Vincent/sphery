#pragma once

#include <memory>
#include <unordered_set>
#include <vector>

#include <cereal/access.hpp>

#include "Descriptor.h"
#include "Fluid.h"
#include "MemoryManager.h"

class ParticleSystem {
public:
    explicit ParticleSystem(PropertySet);
    ParticleSystem(const ParticleSystem &other);
    ParticleSystem operator=(const ParticleSystem &other) = delete;
    ~ParticleSystem();

    bool operator==(const ParticleSystem &other) const;

    Fluid &get_fluid_by_idx(unsigned);

    template <class FluidDescriptor>
    unsigned add_fluid(const FluidPhysicProperties &props, const FluidDescriptor &fd) {
        unsigned fluid_index = 0;
        if (!fluids.empty())
            fluid_index = fluids[fluids.size() - 1].get_index() + 1;
        fluids.emplace_back(fluid_index, descriptor, props, fd);
        descriptor.set_size(memory_manager->get_size());

        descriptor.post_processing(fluids, timestep);
        return fluid_index;
    }

    void iterate();

    [[nodiscard]] const MemoryManager &get_memory_manager() const;
    [[nodiscard]] unsigned long get_iteration() const;
    [[nodiscard]] real get_h() const;

private:
    [[nodiscard]] Descriptor create_descriptor() const;

    friend class cereal::access;

    template <class Archive>
    void save(Archive &ar, [[maybe_unused]] const std::uint32_t version) const {
        ar(cereal::make_nvp("Iteration", iteration),
           cereal::make_nvp("RestDensity", k),
           cereal::make_nvp("UniversalGravityConstant", G),
           cereal::make_nvp("SmoothingLength", h),
           cereal::make_nvp("TimeStep", timestep),
           cereal::make_nvp("ActualProperties", ActualProperties),
           cereal::make_nvp("_MM", memory_manager),
           cereal::make_nvp("_F", fluids));
    }

    template <class Archive>
    void load(Archive &ar, const std::uint32_t version) {

        if (version < actual_version)
            spdlog::warn("Trying to load a file corresponding to a newer version of Sphery");

        if (version > actual_version)
            spdlog::warn("Trying to load a file corresponding to an older version of Sphery");
        ar(iteration, k, G, h, timestep, ActualProperties, memory_manager, fluids);

        descriptor = create_descriptor();
    }

    unsigned long iteration{};

    real k = 1.6;
    real G = 6.674e-11;

    real h = 32;
    real timestep = 1;

    PropertySet ActualProperties{};

    std::shared_ptr<MemoryManager> memory_manager{};
    std::vector<Fluid> fluids{};

    Descriptor descriptor;

public:
    static constexpr std::uint32_t actual_version = 1;
};

CEREAL_CLASS_VERSION(ParticleSystem, ParticleSystem::actual_version);
