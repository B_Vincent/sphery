#include "ParticleSystem.h"
#include "DataSet/DataSetDetailed.h"
#include "Force/All.h"
#include "Force/Effect/All.h"
#include "Integrator/IntegratorVerlet.h"


ParticleSystem::ParticleSystem(PropertySet ps) :
    ActualProperties{PropertySet::Basic | ps},
    memory_manager{std::make_shared<MemoryManager>(ActualProperties)},
    descriptor{create_descriptor()} {

    trace_constructor();
}

ParticleSystem::ParticleSystem(const ParticleSystem &other) :
    iteration{other.iteration},
    k{other.k},
    G{other.G},
    h{other.h},
    timestep{other.timestep},
    ActualProperties{other.ActualProperties},
    memory_manager{std::make_shared<MemoryManager>(*other.memory_manager)},
    fluids{other.fluids},
    descriptor{create_descriptor()} {

    trace_constructor();
}

bool ParticleSystem::operator==(const ParticleSystem &other) const {
    return iteration == other.iteration
           and k == other.k
           and G == other.G
           and h == other.h
           and timestep == other.timestep
           and ActualProperties == other.ActualProperties
           and *memory_manager == *other.memory_manager
           and fluids == other.fluids;
}

ParticleSystem::~ParticleSystem() {
    trace_destructor();
}

void ParticleSystem::iterate() {
    spdlog::trace("Iteration number: {}", iteration);

    descriptor.compute_forces();

    memory_manager->display_statistics(true);

    descriptor.integrate(timestep);
    descriptor.clean_up();

    ++iteration;
}

Fluid &ParticleSystem::get_fluid_by_idx(const unsigned idx) {
    return fluids[idx];
}

unsigned long ParticleSystem::get_iteration() const {
    return iteration;
}

const MemoryManager &ParticleSystem::get_memory_manager() const {
    return *memory_manager;
}
real ParticleSystem::get_h() const {
    return h;
}

Descriptor ParticleSystem::create_descriptor() const {
    std::shared_ptr<DataSet> descriptor_data{};
    if ((ActualProperties & PropertySet::DetailedForces) == PropertySet::DetailedForces)
        descriptor_data = std::make_shared<DataSetDetailed>(memory_manager);
    else
        descriptor_data = std::make_shared<DataSet>(memory_manager);

    std::shared_ptr<Integrator> descriptor_integrator{};
    if ((ActualProperties & PropertySet::Verlet) == PropertySet::Verlet)
        descriptor_integrator = std::make_shared<IntegratorVerlet>(memory_manager);

    std::shared_ptr<Kernel::Kernel> beta_spline = std::make_shared<Kernel::BetaSpline>(h);
    std::shared_ptr<Kernel::Kernel> viscosity = std::make_shared<Kernel::Viscosity>(h);

    using SharedForceVector = std::vector<std::shared_ptr<Force::Force>>;
    using SharedEffectVector = std::vector<std::shared_ptr<Force::Effect::Effect>>;

    SharedForceVector main_forces({std::make_shared<Force::Gravity>(fluids, G),
                                   std::make_shared<Force::Pressure::Desbrun2010>(fluids, beta_spline),
                                   std::make_shared<Force::Viscosity::Monaghan1992>(fluids, viscosity, h, 1., 2.)});

    SharedForceVector density_force({std::make_shared<Force::Density>(fluids, beta_spline)});

    SharedEffectVector density_pressure_effects({std::make_shared<Force::Effect::OwnDensity>(fluids, beta_spline),
                                                 std::make_shared<Force::Effect::Pressure>(fluids, k)});

    SharedForceVector density_pressure_grad_force({std::make_shared<Force::DensityGrad>(fluids, beta_spline),
                                                   std::make_shared<Force::PressureGrad>(fluids, beta_spline)});

    SharedEffectVector sound_speed({std::make_shared<Force::Effect::SoundSpeed>(fluids)});


    return {descriptor_data, descriptor_integrator, memory_manager,
            {
                    {density_force, density_pressure_effects},
                    {density_pressure_grad_force, sound_speed},
                    {main_forces, {}}
            }};
}
