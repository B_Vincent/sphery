#pragma once

#include "DataSet/DataSet.h"
#include "Fluid.h"
#include "Integrator/Integrator.h"
#include "MemoryManager.h"
#include "Pipeline.h"
#include "PostProcess.h"


class Descriptor {
public:
    Descriptor() = delete;
    Descriptor(std::shared_ptr<DataSet> data, std::shared_ptr<Integrator> integrator,
               std::shared_ptr<MemoryManager>,
               std::vector<std::pair<std::vector<std::shared_ptr<Force::Force>>,
                                     std::vector<std::shared_ptr<Force::Effect::Effect>>>>);

    ~Descriptor() = default;

    bool operator==(const Descriptor &other) const = default;

    void compute_forces() const;
    void integrate(real timestep) const;

    void add_particles(Fluid &fluid, const std::vector<vec3> &, const std::vector<vec3> &, AddParticlesMethod) const;
    void post_processing(std::vector<Fluid> &fluids, real timestep) const;
    void clean_up() const;

    void set_size(unsigned long s);

private:
    std::shared_ptr<DataSet> data{};
    std::shared_ptr<Integrator> integrator{};

    Pipeline pipeline;
};
