#pragma once

#include <vector>

#include "AddParticlesMethod.h"
#include "Simulator/Common.h"
#include "Simulator/DataSet/DataSet.h"
#include "Simulator/Fluid.h"
#include "Simulator/MemoryManager.h"
#include "Simulator/Pipeline.h"

std::string AddParticlesMethod_to_string(AddParticlesMethod apm);

class Integrator {
public:
    explicit Integrator(std::shared_ptr<MemoryManager>);
    virtual ~Integrator() = default;

    virtual void integrate(const std::shared_ptr<DataSet> &, real timestep) const = 0;

    virtual void post_processing(const std::shared_ptr<DataSet> &, const Pipeline &, std::vector<Fluid> &fluids, real timestep) const = 0;

    virtual void add_particles(const std::shared_ptr<DataSet> &, Fluid &, const std::vector<vec3> &, const std::vector<vec3> &, AddParticlesMethod) const = 0;

protected:
    std::shared_ptr<MemoryManager> memory_manager;
};

class WrongIntegrator : public std::exception {
public:
    explicit WrongIntegrator(const std::string &name, const AddParticlesMethod apm) :
        msg{fmt::format(R"(Unable to call "{}" with an integrator of type : "{}")", AddParticlesMethod_to_string(apm), name)} {}

    [[nodiscard]] const char *what() const noexcept override {
        return msg.c_str();
    }

private:
    std::string msg;
};
