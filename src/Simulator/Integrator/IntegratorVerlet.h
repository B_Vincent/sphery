#pragma once

#include <vector>

#include "Integrator.h"
#include "Simulator/DataSet/DataSet.h"
#include "Simulator/Fluid.h"
#include "Simulator/MemoryManager.h"

class IntegratorVerlet : public Integrator {
public:
    explicit IntegratorVerlet(const std::shared_ptr<MemoryManager> &mm);

    void integrate(const std::shared_ptr<DataSet> &, real timestep) const final;

    void post_processing(const std::shared_ptr<DataSet> &, const Pipeline &, std::vector<Fluid> &fluids, real timestep) const final;

    void add_particles(const std::shared_ptr<DataSet> &, Fluid &fluid, const std::vector<vec3> &, const std::vector<vec3> &, AddParticlesMethod) const final;

private:
    std::vector<vec3> &positions_old;
};
