#pragma once

enum class AddParticlesMethod {
    VerletOldPos,
    VerletSpeed,
};

