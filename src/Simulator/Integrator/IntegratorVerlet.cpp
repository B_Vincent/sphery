#include "IntegratorVerlet.h"

IntegratorVerlet::IntegratorVerlet(const std::shared_ptr<MemoryManager> &mm) :
    Integrator(mm),
    positions_old{mm->get_vec3_by_name("position_old")} {}


void IntegratorVerlet::integrate(const std::shared_ptr<DataSet> &ds, real timestep) const {
    auto &positions = ds->positions;
    auto &speeds = ds->speeds;

    const auto &forces = ds->forces;
    const auto &masses = ds->masses;

    for (unsigned i = 0; i < ds->size; ++i) {
        const vec3 current_position = positions[i];

        positions[i] = 2 * positions[i] - positions_old[i] + (timestep * timestep) * (forces[i] / masses[i]);
        speeds[i] = (positions_old[i] - positions[i]) / timestep;
        positions_old[i] = current_position;
    }
}

void IntegratorVerlet::post_processing(const std::shared_ptr<DataSet> &ds,
                                       const Pipeline &pipeline,
                                       std::vector<Fluid> &fluids,
                                       real timestep) const {

    for (unsigned index = 0; index < fluids.size(); ++index) {
        PostProcess effect = fluids[index].get_processing_effect();

        if ((effect & PostProcess::ComputeSpeed) == PostProcess::ComputeSpeed) {
            for (unsigned i = 0; i < ds->size; ++i) {
                if (static_cast<unsigned>(ds->fluids[i]) == index)
                    ds->speeds[i] = (ds->positions[i] - positions_old[i]) / timestep;
            }

            fluids[index].remove_processing_effect(PostProcess::ComputeSpeed);
        }

        if ((effect & PostProcess::ComputeOldPos) == PostProcess::ComputeOldPos) {

            pipeline.execute(ds);

            for (unsigned i = 0; i < ds->size; ++i) {
                if (static_cast<unsigned>(ds->fluids[i]) == index) {
                    positions_old[i] =
                            ds->positions[i] - ds->speeds[i] * timestep - 1 / 2 * ds->forces[i] / ds->masses[i] * square::pow(timestep);
                }
            }

            ds->clean_up();

            fluids[index].remove_processing_effect(PostProcess::ComputeOldPos);
        }
    }
}

void IntegratorVerlet::add_particles(const std::shared_ptr<DataSet> &ds,
                                     Fluid &fluid,
                                     const std::vector<vec3> &pos,
                                     const std::vector<vec3> &additional,
                                     const AddParticlesMethod apm) const {

    const auto offset = memory_manager->get_size();
    const auto nb_particles = pos.size();

    memory_manager->add_particle(nb_particles);

    switch (apm) {
        case AddParticlesMethod::VerletOldPos:
            for (unsigned i{}; i < nb_particles; ++i) {
                ds->masses[offset + i] = fluid.get_particle_mass();
                ds->positions[offset + i] = pos[i];
                positions_old[offset + i] = additional[i];
                ds->fluids[offset + i] = fluid.get_index();
            }
            fluid.add_processing_effect(PostProcess::ComputeSpeed);
            break;

        case AddParticlesMethod::VerletSpeed:
            for (unsigned i{}; i < nb_particles; ++i) {
                ds->masses[offset + i] = fluid.get_particle_mass();
                ds->positions[offset + i] = pos[i];
                ds->speeds[offset + i] = additional[i];
                ds->fluids[offset + i] = fluid.get_index();
            }
            fluid.add_processing_effect(PostProcess::ComputeOldPos);
            break;

        default:
            throw WrongIntegrator{"IntegratorVerlet", apm};
    }
}
