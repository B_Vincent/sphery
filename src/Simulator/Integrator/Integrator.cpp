#include <random>
#include <source_location>
#include <utility>

#include "Integrator.h"

std::string AddParticlesMethod_to_string(const AddParticlesMethod apm) {
    switch (apm) {
        case AddParticlesMethod::VerletSpeed:
            return "Verlet - Speed";

        case AddParticlesMethod::VerletOldPos:
            return "Verlet - OldPos";

        default:
            throw std::logic_error(fmt::format("Need update at {} : {}", std::source_location::current().file_name(), std::source_location::current().function_name()));
    }
}

Integrator::Integrator(std::shared_ptr<MemoryManager> mm) :
    memory_manager{std::move(mm)} {}
