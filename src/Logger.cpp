#include "Logger.h"

void trace_constructor(std::source_location sl) {
    spdlog::trace("Initializing {}", sl.function_name());
}

void trace_destructor(std::source_location sl) {
    spdlog::trace("Destructing {}", sl.function_name());
}

void trace(std::source_location sl) {
    spdlog::trace("Passed {}:{}:{} in function : {}",
                  sl.file_name(),
                  sl.line(),
                  sl.column(),
                  sl.function_name());
}

Logger::Logger(spdlog::level::level_enum log_level, bool display, const std::optional<std::string> &filename) {
    const auto sinks_list = [display, &filename]() {
        std::vector<std::shared_ptr<spdlog::sinks::sink>> tmp_sink_list;

        if (filename.has_value())
            tmp_sink_list.emplace_back(std::make_unique<spdlog::sinks::basic_file_sink_mt>(filename.value(), true));

        if (display)
            tmp_sink_list.emplace_back(std::make_unique<spdlog::sinks::stdout_color_sink_mt>());

        return tmp_sink_list;
    }();


    const auto common_logger = std::make_shared<spdlog::logger>(
            "full_logger",
            std::cbegin(sinks_list), std::cend(sinks_list));

    spdlog::set_default_logger(common_logger);

    spdlog::set_level(log_level);

    // log pattern : https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
    spdlog::set_pattern("[%H:%M:%S.%2!e] [%^%l%$] %v");

    spdlog::info("Logger initialized");
}

Logger::~Logger() {
    spdlog::info("Exit");
}
