#pragma once

#include <optional>
#include <source_location>
#include <string_view>

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>


void trace_constructor(std::source_location sl = std::source_location::current());
void trace_destructor(std::source_location sl = std::source_location::current());
void trace(std::source_location sl = std::source_location::current());


class Logger {
public:
    Logger(spdlog::level::level_enum log_level, bool display, const std::optional<std::string>& filename);
    ~Logger();
};
