#include "Logger.h"

#include "Simulator/ParticleSystem.h"
#include "Simulator/Saver/Exporter.h"


int main() {
    const Logger global_logger{spdlog::level::level_enum::trace, true, {"logs/basic.log"}};

    ParticleSystem particle_system{PropertySet::DetailedForces | PropertySet::Verlet};


    // Initial parameters
    const real rho0 = .83;
    const real mass = 20;
    const real c = 1000;

    const vec3 position{0, 0, 0};
    const real radius = 100;
    const real distance_particle = particle_system.get_h() / 4;

    const VolumeGeneration::SphereDescriptor sd{position,
                                                distance_particle,
                                                radius};

    const FluidPhysicProperties fluid_properties{
            .rho0 = rho0,
            .particle_mass = mass,
            .fluid_mass = {},
            .speed_of_sound = c,
            .jiggle_on_h = distance_particle / 16,
            .modifier_prop = FluidModifierProperties{Modifier{
                                                             .offset = {0, 0, 0},
                                                             .rotation = Rotation{.axis{0, 0, 1},
                                                                                  .angle = 0},
                                                     },
                                                     Modifier{
                                                             .offset = {1, 0, 0},
                                                             .rotation = Rotation{.axis{0, 0, 0},
                                                                                  .angle = 0},
                                                     }}};


    particle_system.add_fluid(fluid_properties, sd);

    particle_system.get_memory_manager().display_properties();

    constexpr unsigned final = 100;
    const std::string directory{"../results/"};
    {
        const Exporter exporter(particle_system, fmt::format(directory + "/test_{}.txt", particle_system.get_iteration()));
    }
    for (unsigned i = 0; i < final; ++i) {
        particle_system.iterate();
        const Exporter exporter(particle_system, fmt::format(directory + "/test_{}.txt", particle_system.get_iteration()));
    }

    return 0;
}
