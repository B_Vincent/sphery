#include <vector>

#include "Simulator/Common.h"
#include "VolumeFilling.h"

namespace VolumeGeneration {

    Box box_from_sphere_descriptor(const SphereDescriptor &sd) {
        constexpr real length = half_cube_length * std::numbers::sqrt3_v<real>;
        const vec3 offset = sd.radius * length * vec3{1, 1, 1};

        const vec3 min = sd.position - offset;
        const vec3 max = sd.position + offset;

        return {min, max};
    }

    Box box_from_cuboid_descriptor(const CuboidDescriptor &cd) {
        const vec3 offset = {cd.length / 2, cd.height / 2, cd.width / 2};

        const vec3 min = cd.position - offset;
        const vec3 max = cd.position + offset;

        return {min, max};
    }

    real signed_distance_sphere(const vec3 &pos, const SphereDescriptor &sphere) {
        return (pos - sphere.position).norm() - sphere.radius;
    }

    real signed_distance_cuboid(const vec3 &pos, const CuboidDescriptor &cuboid) {
        const vec3 q = (pos - cuboid.position).cwiseAbs() - vec3{cuboid.length, cuboid.height, cuboid.width};
        return q.cwiseMax(vec3{0, 0, 0}).norm() + std::min(std::max(q.x(), std::max(q.y(), q.z())), 0.0);
    }

}// namespace VolumeGeneration
