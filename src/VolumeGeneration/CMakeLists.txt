add_library(VolumeGeneration STATIC
        BoundingVolumeHierarchy.cpp BoundingVolumeHierarchy.h
        AssimpInterface.cpp AssimpInterface.h
        Primitive.cpp Primitive.h
        VolumeFilling.cpp VolumeFilling.h
        )

target_opt_and_lib(VolumeGeneration)

target_link_libraries(VolumeGeneration PUBLIC assimp)
