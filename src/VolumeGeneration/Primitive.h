#pragma once

#include <algorithm>
#include <cmath>
#include <numeric>

#include <assimp/vector3.h>


#include "Simulator/Common.h"

namespace VolumeGeneration {

    inline vec3 aiV3_to_vec3(const aiVector3D &v) {
        return {v.x, v.y, v.z};
    }

    template <unsigned long N>
    inline std::array<vec3, N> aiV3_to_vec3(const std::array<aiVector3D, N> &v) {
        std::array<vec3, N> ret;
        std::transform(cbegin(v), cend(v), begin(ret),
                       [](const aiVector3D &v) -> vec3 { return aiV3_to_vec3(v); });
        return ret;
    }

    // https://stackoverflow.com/questions/14997165/fastest-way-to-get-a-positive-modulo-in-c-c
    inline unsigned positive_modulo(int value, unsigned m) {
        int mod = value % (int) m;
        if (mod < 0) {
            mod += static_cast<int>(m);
        }
        return static_cast<unsigned>(mod);
    }

    // https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
    template <typename T>
    int sign(T val) {
        return (T(0) < val) - (val < T(0));
    }

    // Implementation from https://www.iquilezles.org/www/articles/triangledistance/triangledistance.htm
    inline real dot2(const vec3 &v) { return v.dot(v); }

    template <unsigned N>
    class Primitive {
    public:
        Primitive(const std::array<vec3, N> &p,
                  const std::array<vec3, N> &p_n) :
            points(p),
            points_normals(p_n) {
            compute_centroid();
            compute_face_normal();
            compute_edges_normals();
        }

        [[nodiscard]] const vec3 &get_centroid() const {
            return centroid;
        }

        [[nodiscard]] const vec3 &get_face_normal() const {
            return face_normal;
        }

        [[nodiscard]] const auto &get_points() const {
            return points;
        }

        [[nodiscard]] const auto &get_points_normals() const {
            return points_normals;
        }

        [[nodiscard]] const auto &get_edges_normals() const {
            return edges_normals;
        }

        void set_point_normal(unsigned idx, const vec3 &v) {
            points_normals[idx] = v;
        }

        friend real signed_distance_to_triangle(const vec3 &pos, const Primitive<3> &t);
        template <unsigned Size>
        friend void compute_points_normals(std::vector<Primitive<Size>> &primitives);
        template <unsigned Size>
        friend void extend_box(Box &box, const VolumeGeneration::Primitive<Size> &t);

    private:
        void compute_centroid() {
            centroid = std::reduce(begin(points), end(points)) / N;
        }

        void compute_face_normal() {
            // We should divide the sum by N, but it's useless as we normalize after
            const auto signed_face_normal = std::reduce(begin(points_normals), end(points_normals));
            const auto directional_face_normal = ((points[1] - points[0]).cross(points[2] - points[1])).normalized();
            face_normal = sign(signed_face_normal.dot(directional_face_normal)) * directional_face_normal;
        }

        void compute_edges_normals() {
            for (unsigned i{}; i < N; ++i) {
                const vec3 current_edge = points[(i + 1) % N] - points[i];
                const vec3 next_edge = points[(i + 2) % N] - points[(i + 1) % N];
                edges_normals[i] = ((current_edge).cross(current_edge.cross(next_edge))).normalized();
            }
        }

        [[nodiscard]] real inner_vertices_angle(const unsigned vertex) const {
            const vec3 x1 = points[positive_modulo(static_cast<int>(vertex) - 1, N)] - points[vertex];
            const vec3 x2 = points[positive_modulo(static_cast<int>(vertex) + 1, N)] - points[vertex];
            
            return std::acos(x1.dot(x2) / (x1.norm() * x2.norm()));
        }
        
        std::array<vec3, N> points{};
        std::array<vec3, N> points_normals{};

        std::array<vec3, N> edges_normals{};

        vec3 centroid{};
        vec3 face_normal{};
    };

    using Triangle = Primitive<3>;
    real signed_distance_to_triangle(const vec3 &pos, const Triangle &t);

    /**
     * Angle weighted pseudo-normal as described in Bærentzen & Aanæs 2005
     *
     */
    template <unsigned N>
    void compute_points_normals(std::vector<Primitive<N>> &primitives) {
        std::unordered_map<vec3, unsigned> computed_normals;
        std::vector<vec3> summed_normals;

        for (const auto &prim : primitives) {
            for (unsigned j{}; j < prim.points.size(); ++j) {
                if (computed_normals.count(prim.points[j]) == 0) {
                    computed_normals.emplace(prim.points[j], summed_normals.size());
                    summed_normals.emplace_back(vec3{});
                }

                const real a_i = prim.inner_vertices_angle(j);

                summed_normals[computed_normals[prim.points[j]]] += a_i * prim.face_normal;
            }
        }

        for (auto &prim : primitives) {
            for (unsigned i{}; i < prim.points.size(); ++i)
                prim.set_point_normal(i, (summed_normals[computed_normals[prim.points[i]]]).normalized());
        }
    }

    template <unsigned PrimitiveSize>
    inline void
    extend_box(Box &box, const VolumeGeneration::Primitive<PrimitiveSize> &t) {
        for (const auto &point : t.points)
            box.extend(point);
    }

}// namespace VolumeGeneration
