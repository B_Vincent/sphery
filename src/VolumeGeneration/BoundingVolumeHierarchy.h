#pragma once

#include <stack>
#include <vector>

#include <Eigen/Geometry>

#include "AssimpInterface.h"
#include "Logger.h"
#include "Primitive.h"
#include "Simulator/Common.h"

namespace VolumeGeneration {

    unsigned get_max_dim(const Box &);

    /**
     * BoundingVolumeHierarchy based on the one described in Physically Based Rendering
     *
     */
    template <typename Primitive>
    class BoundingVolumeHierarchy {
    private:
        class Node {
        public:
            Node(unsigned long start, unsigned long stop);
            Node(const Node &) = delete;
            Node operator=(const Node &) = delete;
            ~Node();

            [[nodiscard]] bool is_leaf() const {
                return child_1 == nullptr && child_2 == nullptr;
            }

            Box box{};

            Node *child_1 = nullptr;
            Node *child_2 = nullptr;

            unsigned long start = 0, stop = 0;
        };

        void subdivide();

        std::vector<Primitive> primitives{};
        Node root;

    public:
        explicit BoundingVolumeHierarchy(std::vector<Primitive>);
        explicit BoundingVolumeHierarchy(const std::string &filename);

        [[nodiscard]] vec3 get_center() const;
        [[nodiscard]] Box get_main_box() const;

        [[nodiscard]] real signed_distance(const vec3 &pos) const;
    };

    template <typename T>
    BoundingVolumeHierarchy<T>::BoundingVolumeHierarchy(std::vector<T> p) :
        primitives(p),
        root{0, primitives.size()} {
        for (unsigned i = 0; i < primitives.size(); ++i) {
            extend_box(root.box, primitives[i]);
        }
        subdivide();
    }

    template <typename T>
    BoundingVolumeHierarchy<T>::BoundingVolumeHierarchy(const std::string &filename) :
        primitives(load_primitives_from_object(filename)),
        root{0, primitives.size()} {
        for (unsigned i = 0; i < primitives.size(); ++i) {
            extend_box(root.box, primitives[i]);
        }
        subdivide();
    }

    template <typename T>
    BoundingVolumeHierarchy<T>::Node::Node(unsigned long start, unsigned long stop) :
        start(start),
        stop(stop) {}

    template <typename T>
    BoundingVolumeHierarchy<T>::Node::~Node() {
        delete child_1;
        delete child_2;
    }

    template <typename T>
    vec3 BoundingVolumeHierarchy<T>::get_center() const {
        return root.box.center();
    }

    template <typename T>
    Box BoundingVolumeHierarchy<T>::get_main_box() const {
        return root.box;
    }

    /**
     * BVH Best Median building strategy as described in Fryazinov & Pasko 2013
     *
     */
    template <typename Primitive>
    void BoundingVolumeHierarchy<Primitive>::subdivide() {
        spdlog::info("Constructing BoundingVolumeHierarchy");

        std::stack<Node *> waiting_nodes{};
        waiting_nodes.push(&root);

        while (!waiting_nodes.empty()) {
            Node *current = waiting_nodes.top();
            waiting_nodes.pop();

            const auto nb_primitives = current->stop - current->start;

            if (nb_primitives == 0) {
                spdlog::warn("Something went wrong in BVH construction");
            }

            for (auto i = current->start; i < current->stop; ++i)
                extend_box(current->box, primitives[i]);

            if (nb_primitives == 1)
                continue;

            const Box centroids_hull = [&current, &primitives = this->primitives]() -> Box {
                Box tmp{};
                for (auto i = current->start; i < current->stop; ++i)
                    tmp.extend(primitives[i].get_centroid());
                return tmp;
            }();


            const unsigned best_dim = get_max_dim(centroids_hull);
            const real centroid_mean = (centroids_hull.min()[best_dim] + centroids_hull.max()[best_dim]) / 2;

            const auto inf_to_mean = [best_dim, centroid_mean](const Primitive &p) {
                return p.get_centroid()[best_dim] < centroid_mean;
            };

            unsigned long median = std::partition(&primitives[current->start], &primitives[current->stop], inf_to_mean)
                                   - primitives.data();

            // FIXME : In some case primitives can't be partitioned
            //  by the median. The following is a workaround to
            //  force a division of the node. And median should be const.
            if (median == current->start)
                ++median;

            current->child_1 = new Node(current->start, median);
            current->child_2 = new Node(median, current->stop);

            waiting_nodes.push(current->child_1);
            waiting_nodes.push(current->child_2);
        }

        spdlog::info("BoundingVolumeHierarchy constructed");
    }

}// namespace VolumeGeneration
