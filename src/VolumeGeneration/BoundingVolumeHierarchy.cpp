#include "BoundingVolumeHierarchy.h"

namespace VolumeGeneration{

    unsigned get_max_dim(const Box& box){
        const auto d = box.diagonal();
        if (d.x() > d.y() && d.x() > d.z())
            return 0;
        if (d.y() > d.z())
            return 1;
        return 2;
    }

    /**
     *  BVH priority queue traversal as described by Guéziec 2001
     *
     *  For now, we don't implement the notion of priority in our queue.
     *  This should not change anything as we only query for exact result
     *  and not approximations.
     *  We should also implement a cache along with the queue to store result
     *  from previous call.
     */
    template <>
    real BoundingVolumeHierarchy<Triangle>::signed_distance(const vec3 &pos) const {
        std::stack<const Node *> priority_queue{};
        priority_queue.push(&root);
        real shortest_distance = signed_distance_to_triangle(pos, primitives[0]);

        while (!priority_queue.empty()) {
            const Node &current = *priority_queue.top();
            priority_queue.pop();
            if (current.is_leaf()) {
                const real distance = signed_distance_to_triangle(pos, primitives[current.start]);
                if (std::abs(distance) < std::abs(shortest_distance)) {
                    shortest_distance = distance;
                }
            } else {
                if (current.box.exteriorDistance(pos) < std::abs(shortest_distance)) {

                    priority_queue.push(current.child_1);
                    priority_queue.push(current.child_2);
                }
            }
        }

        return shortest_distance;
    }
}