#pragma once

#include <assimp/scene.h>
#include <assimp/Importer.hpp>

#include "Primitive.h"

namespace VolumeGeneration {
    void processFaces(aiMesh *mesh, std::vector<VolumeGeneration::Triangle>& primitives);
    void processNode(aiNode *node, const aiScene *scene, std::vector<VolumeGeneration::Triangle>& primitives);
    const aiScene* import_object(Assimp::Importer& importer, const std::string& pFile);

    std::vector<Triangle> load_primitives_from_object(const std::string& filename);
}
