#pragma once

#include <numbers>
#include <source_location>
#include <unordered_set>
#include <vector>

#include "Logger.h"
#include "Simulator/Common.h"

namespace VolumeGeneration {

    template <typename T>
    concept SignedDistanceFunction = requires(T a, vec3 b) {
        { a(b) } -> std::convertible_to<real>;
    };

    struct VolumeDescriptor {
        vec3 position{0, 0, 0};
        real distance_particle{};
    };

    template <SignedDistanceFunction SDF>
    struct ObjectDescriptor : public VolumeDescriptor {
        const SDF sdf;
        const Box box;
    };

    struct CuboidDescriptor : public VolumeDescriptor {
        CuboidDescriptor(const vec3 &pos, real d_p, real len, real hei, real wid) :
            VolumeDescriptor{pos, d_p}, length{len}, height{hei}, width{wid} {}
        real length = 1;
        real height = 1;
        real width = 1;
    };

    struct SphereDescriptor : public VolumeDescriptor {
        SphereDescriptor(const vec3 &pos, real d_p, real rad) :
            VolumeDescriptor{pos, d_p}, radius{rad} {}
        real radius = 1;
    };

    [[nodiscard]] Box box_from_sphere_descriptor(const SphereDescriptor&);
    [[nodiscard]] Box box_from_cuboid_descriptor(const CuboidDescriptor&);

    [[nodiscard]] real signed_distance_sphere(const vec3 &, const SphereDescriptor &);
    [[nodiscard]] real signed_distance_cuboid(const vec3 &, const CuboidDescriptor &);

    constexpr real half_cube_length = std::numbers::sqrt2_v<real>;
    const std::vector<vec3> face_centered_cubic_lattice = {
            half_cube_length * vec3{1, 0, 0}, half_cube_length *vec3{-1, 0, 0},
            half_cube_length *vec3{0, 1, 0}, half_cube_length *vec3{0, -1, 0},
            half_cube_length *vec3{0, 0, 1}, half_cube_length *vec3{0, 0, -1},

            half_cube_length *vec3{1, 1, 1}, half_cube_length *vec3{1, -1, 1},
            half_cube_length *vec3{1, 1, -1}, half_cube_length *vec3{1, -1, -1},
            half_cube_length *vec3{-1, 1, 1}, half_cube_length *vec3{-1, -1, 1},
            half_cube_length *vec3{-1, 1, -1}, half_cube_length *vec3{-1, -1, -1}};

    const std::vector<vec3> potential_children = {
            2 * half_cube_length * vec3{1, 0, 0},
            2 * half_cube_length *vec3{-1, 0, 0},
            2 * half_cube_length *vec3{0, 1, 0},
            2 * half_cube_length *vec3{0, -1, 0},
            2 * half_cube_length *vec3{0, 0, 1},
            2 * half_cube_length *vec3{0, 0, -1},
    };

    template <SignedDistanceFunction SDF>
    void place_points(std::unordered_set<vec3, NormBasedHash<vec3>> &points, const SDF &sdf,
                      const vec3 &center, const real h) {
        for (auto x : face_centered_cubic_lattice) {
            x = x * h + center;
            if (sdf(x) < 0)
                points.insert(x);
        }
    }

    template <SignedDistanceFunction SDF>
    bool add_lattice(std::unordered_set<vec3, NormBasedHash<vec3>> &lattice,
                     std::unordered_set<vec3, NormBasedHash<vec3>> &points,
                     const SDF &sdf, const vec3 &center, const real h) {
        const real dist = sdf(center);

        // The entire lattice is outside
        if (dist - (std::sqrt(6) + 1) * h > 0)
            return false;

        // A part of the lattice is outside
        if (dist + (std::sqrt(6) + 1) * h > 0) {
            place_points(points, sdf, center, h);
        } else {
            // The lattice is totally inside
            for (const auto &x : face_centered_cubic_lattice)
                points.insert(center + h * x);
        }

        lattice.insert(center);
        return true;
    }


    template <SignedDistanceFunction SDF>
    void place_children(std::unordered_set<vec3, NormBasedHash<vec3>> &lattice,
                        std::unordered_set<vec3, NormBasedHash<vec3>> &points,
                        const SDF &sdf, const vec3 &center, const real h) {
        for (auto v : potential_children) {
            v = v * h + center;

            if (lattice.count(v) == 0 && add_lattice(lattice, points, sdf, v, h))
                place_children(lattice, points, sdf, v, h);
        }
    }

    enum class FillingMethod{Cubic,
                                };

    template <SignedDistanceFunction SDF>
    void add_object(std::vector<vec3> &v, const Box &box, const SDF &sdf, const real h, FillingMethod fm = FillingMethod::Cubic) {
        std::vector<vec3> tmp;

        if (fm == FillingMethod::Cubic){
            const auto diff = box.max() - box.min();

            const auto x_length_max = static_cast<unsigned>(diff[0] / h);
            const auto y_length_max = static_cast<unsigned>(diff[1] / h);
            const auto z_length_max = static_cast<unsigned>(diff[2] / h);

            spdlog::debug("Adding (worst case) {} particles", x_length_max * y_length_max * z_length_max);


            for (unsigned i = 0; i < x_length_max; ++i) {
                for (unsigned j = 0; j < y_length_max; ++j) {
                    for (unsigned k = 0; k < z_length_max; ++k) {
                        const vec3 current_pos = {
                                box.min()[0] + h * i,
                                box.min()[1] + h * j,
                                box.min()[2] + h * k};
                        if (sdf(current_pos) < 0) {
                            tmp.push_back(current_pos);
                        }
                    }
                }
            }
        }

        v.reserve(v.size() + tmp.size());
        std::move(std::begin(tmp), std::end(tmp), std::back_inserter(v));
    }

}// namespace VolumeGeneration
