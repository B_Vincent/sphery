#include <assimp/postprocess.h>

#include "AssimpInterface.h"
#include "Logger.h"

namespace VolumeGeneration {

    void processFaces(aiMesh *mesh, std::vector<VolumeGeneration::Triangle> &primitives) {

        if (!mesh->HasNormals())
            spdlog::warn("Unable to find normals");

        primitives.reserve(primitives.size() + mesh->mNumFaces);
        for (unsigned i = 0; i < mesh->mNumFaces; i++) {
            aiFace face = mesh->mFaces[i];
            if (face.mNumIndices != 3)
                spdlog::warn("The shape is not a triangle");

            primitives.emplace_back(
                    VolumeGeneration::aiV3_to_vec3(
                            std::array<aiVector3D, 3>{
                                    mesh->mVertices[face.mIndices[0]],
                                    mesh->mVertices[face.mIndices[1]],
                                    mesh->mVertices[face.mIndices[2]]}),
                    VolumeGeneration::aiV3_to_vec3(
                            std::array<aiVector3D, 3>{
                                    mesh->mNormals[face.mIndices[0]],
                                    mesh->mNormals[face.mIndices[1]],
                                    mesh->mNormals[face.mIndices[2]]}));
        }
    }

    void processNode(aiNode *node, const aiScene *scene,
                     std::vector<VolumeGeneration::Triangle> &primitives) {
        // process all the node's meshes (if any)
        spdlog::debug("Node {} : {}", node->mName.data, (void *) node);
        for (unsigned i = 0; i < node->mNumMeshes; i++) {
            aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
            processFaces(mesh, primitives);
        }
        // then do the same for each of its children
        for (unsigned i = 0; i < node->mNumChildren; i++) {
            processNode(node->mChildren[i], scene, primitives);
        }
    }

    const aiScene *import_object(Assimp::Importer &importer, const std::string &pFile) {
        const aiScene *scene = importer.ReadFile(pFile,
                                                 aiProcess_CalcTangentSpace |                // calculate tangents and bitangents if possible
                                                         aiProcess_JoinIdenticalVertices |   // join identical vertices/ optimize indexing
                                                         aiProcess_ValidateDataStructure |   // perform a full validation of the loader's output
                                                         aiProcess_Triangulate |             // Ensure all verticies are triangulated (each 3 vertices are triangle)
                                                         aiProcess_SortByPType |             // ?
                                                         aiProcess_ImproveCacheLocality |    // improve the cache locality of the output vertices
                                                         aiProcess_RemoveRedundantMaterials |// remove redundant materials
                                                         aiProcess_FindDegenerates |         // remove degenerated polygons from the import
                                                         aiProcess_FindInvalidData |         // detect invalid model data, such as invalid normal vectors
                                                         aiProcess_GenUVCoords |             // convert spherical, cylindrical, box and planar mapping to proper UVs
                                                         aiProcess_TransformUVCoords |       // preprocess UV transformations (scaling, translation ...)
                                                         aiProcess_FindInstances |           // search for instanced meshes and remove them by references to one master
                                                         aiProcess_LimitBoneWeights |        // limit bone weights to 4 per vertex
                                                         aiProcess_OptimizeMeshes |          // join small meshes, if possible;
                                                         aiProcess_GenNormals | 0);

        if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
            throw std::runtime_error(importer.GetErrorString());
        }

        return scene;
    }

    std::vector<Triangle> load_primitives_from_object(const std::string &filename) {
        Assimp::Importer imp;
        auto scene = import_object(imp, filename);

        spdlog::info("Scene Meshes : {}", scene->mNumMeshes);

        std::vector<VolumeGeneration::Triangle> primitives;

        spdlog::debug("Root node : {}", (void *) scene->mRootNode);

        processNode(scene->mRootNode, scene, primitives);
        spdlog::info("Primitives Loaded : {}", primitives.size());

        compute_points_normals(primitives);
        return primitives;
    }

}// namespace VolumeGeneration
