#include "Primitive.h"

namespace VolumeGeneration {

    real signed_distance_to_triangle(const vec3 &pos, const Primitive<3> &t) {
        const vec3 &v1 = t.points[0];
        const vec3 &v2 = t.points[1];
        const vec3 &v3 = t.points[2];

        const vec3 v21 = v2 - v1;
        const vec3 p1 = pos - v1;

        const vec3 v32 = v3 - v2;
        const vec3 p2 = pos - v2;

        const vec3 v13 = v1 - v3;
        const vec3 p3 = pos - v3;

        const vec3 nor = t.face_normal;

        const bool is_inside_prism = std::abs(sign((v21.cross(nor)).dot(p1)) + sign((v32.cross(nor)).dot(p2)) + sign((v13.cross(nor)).dot(p3))) >= 2.0;
        if (is_inside_prism) {
            return t.face_normal.dot(p1);
        }

        const real s1 = std::clamp(v21.dot(p1) / dot2(v21), 0.0, 1.0);
        const vec3 vp1 = v21 * s1 - p1;
        const real d1 = vp1.norm();

        const real s2 = std::clamp(v32.dot(p2) / dot2(v32), 0.0, 1.0);
        const vec3 vp2 = v32 * s2 - p2;
        const real d2 = vp2.norm();

        const real s3 = std::clamp(v13.dot(p3) / dot2(v13), 0.0, 1.0);
        const vec3 vp3 = v13 * s3 - p3;
        const real d3 = vp3.norm();


        // Related to edge 1
        if (d1 < d2 && d1 < d3) {
            if (s1 == 0)
                return std::copysign(d1, p2.dot(t.points_normals[0]));

            if (s1 == 1)
                return std::copysign(d1, p1.dot(t.points_normals[1]));

            return std::copysign(d1, - vp1.dot(t.edges_normals[0]));
        }

        // Related to edge 2
        if (d2 < d3) {
            if (s2 == 0)
                return std::copysign(d2, p3.dot(t.points_normals[1]));

            if (s2 == 1)
                return std::copysign(d2, p2.dot(t.points_normals[2]));

            return std::copysign(d2, - vp2.dot(t.edges_normals[1]));
        }

        // Related to edge 3
        if (s3 == 0)
            return std::copysign(d3, p1.dot(t.points_normals[2]));

        if (s3 == 1)
            return std::copysign(d3, p3.dot(t.points_normals[0]));

        return std::copysign(d3, - vp3.dot(t.edges_normals[2]));
    }


}// namespace VolumeGeneration
